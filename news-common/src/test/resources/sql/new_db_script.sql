--------------------------------------------------------
--  File created - Wednesday-February-17-2016   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table AUTHORS
--------------------------------------------------------

  CREATE TABLE "VITALYA"."AUTHORS" 
   (	"AUTH_ID" NUMBER(20,0), 
	"AUTH_NAME" NVARCHAR2(30), 
	"AUTH_EXPIRED" TIMESTAMP (6)
   ) 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table COMMENTS
--------------------------------------------------------

  CREATE TABLE "VITALYA"."COMMENTS" 
   (	"CMT_ID" NUMBER(20,0), 
	"CMT_NEWS_ID" NUMBER(20,0), 
	"CMT_TEXT" NVARCHAR2(100), 
	"CMT_CREATION_DATE" TIMESTAMP (6)
   )
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table NEWS
--------------------------------------------------------

  CREATE TABLE "VITALYA"."NEWS" 
   (	"NEWS_ID" NUMBER(20,0), 
	"NEWS_TITLE" NVARCHAR2(30), 
	"NEWS_SHORT_TEXT" NVARCHAR2(100), 
	"NEWS_FULL_TEXT" NVARCHAR2(2000), 
	"NEWS_CREATION_DATE" TIMESTAMP (6), 
	"NEWS_MODIFICATION_DATE" DATE
   ) 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table NEWS_AUTHORS
--------------------------------------------------------

  CREATE TABLE "VITALYA"."NEWS_AUTHORS" 
   (	"NA_NEWS_ID" NUMBER(20,0), 
	"NA_AUTHOR_ID" NUMBER(20,0)
   )
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table NEWS_TAGS
--------------------------------------------------------

  CREATE TABLE "VITALYA"."NEWS_TAGS" 
   (	"NT_NEWS_ID" NUMBER(20,0), 
	"NT_TAG_ID" NUMBER(20,0)
   ) 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table ROLES
--------------------------------------------------------

  CREATE TABLE "VITALYA"."ROLES" 
   (	"RL_USER_ID" NUMBER(20,0), 
	"RL_ROLE_NAME" VARCHAR2(50 BYTE), 
	"RL_ID" NUMBER(20,0)
   )
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table TAGS
--------------------------------------------------------

  CREATE TABLE "VITALYA"."TAGS" 
   (	"TAG_ID" NUMBER(20,0), 
	"TAG_NAME" NVARCHAR2(30)
   ) 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table USERS
--------------------------------------------------------

  CREATE TABLE "VITALYA"."USERS" 
   (	"USR_ID" NUMBER(20,0), 
	"USR_NAME" NVARCHAR2(50), 
	"USR_LOGIN" VARCHAR2(30 BYTE), 
	"USR_PASSWORD" VARCHAR2(30 BYTE)
   ) 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index AUTHOR_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "VITALYA"."AUTHOR_PK" ON "VITALYA"."AUTHORS" ("AUTH_ID") 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index COMMENTS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "VITALYA"."COMMENTS_PK" ON "VITALYA"."COMMENTS" ("CMT_ID") 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index NEWS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "VITALYA"."NEWS_PK" ON "VITALYA"."NEWS" ("NEWS_ID") 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index ROLES_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "VITALYA"."ROLES_PK" ON "VITALYA"."ROLES" ("RL_ID") 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index TAG_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "VITALYA"."TAG_PK" ON "VITALYA"."TAGS" ("TAG_ID") 
 TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index USERS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "VITALYA"."USERS_PK" ON "VITALYA"."USERS" ("USR_ID") 
 TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index LOGIN_UQ
--------------------------------------------------------

  CREATE UNIQUE INDEX "VITALYA"."LOGIN_UQ" ON "VITALYA"."USERS" ("USR_LOGIN") 
 TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table AUTHORS
--------------------------------------------------------

  ALTER TABLE "VITALYA"."AUTHORS" MODIFY ("AUTH_NAME" NOT NULL ENABLE);
  ALTER TABLE "VITALYA"."AUTHORS" ADD CONSTRAINT "AUTH_PK" PRIMARY KEY ("AUTH_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "VITALYA"."AUTHORS" MODIFY ("AUTH_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table COMMENTS
--------------------------------------------------------

  ALTER TABLE "VITALYA"."COMMENTS" ADD CONSTRAINT "CMT_PK" PRIMARY KEY ("CMT_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "VITALYA"."COMMENTS" MODIFY ("CMT_CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "VITALYA"."COMMENTS" MODIFY ("CMT_TEXT" NOT NULL ENABLE);
  ALTER TABLE "VITALYA"."COMMENTS" MODIFY ("CMT_NEWS_ID" NOT NULL ENABLE);
  ALTER TABLE "VITALYA"."COMMENTS" MODIFY ("CMT_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS
--------------------------------------------------------

  ALTER TABLE "VITALYA"."NEWS" MODIFY ("NEWS_FULL_TEXT" NOT NULL ENABLE);
  ALTER TABLE "VITALYA"."NEWS" MODIFY ("NEWS_SHORT_TEXT" NOT NULL ENABLE);
  ALTER TABLE "VITALYA"."NEWS" MODIFY ("NEWS_TITLE" NOT NULL ENABLE);
  ALTER TABLE "VITALYA"."NEWS" MODIFY ("NEWS_ID" NOT NULL ENABLE);
  ALTER TABLE "VITALYA"."NEWS" MODIFY ("NEWS_MODIFICATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "VITALYA"."NEWS" MODIFY ("NEWS_CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "VITALYA"."NEWS" ADD CONSTRAINT "NEWS_PK" PRIMARY KEY ("NEWS_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table NEWS_AUTHORS
--------------------------------------------------------

  ALTER TABLE "VITALYA"."NEWS_AUTHORS" MODIFY ("NA_AUTHOR_ID" NOT NULL ENABLE);
  ALTER TABLE "VITALYA"."NEWS_AUTHORS" MODIFY ("NA_NEWS_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS_TAGS
--------------------------------------------------------

  ALTER TABLE "VITALYA"."NEWS_TAGS" MODIFY ("NT_TAG_ID" NOT NULL ENABLE);
  ALTER TABLE "VITALYA"."NEWS_TAGS" MODIFY ("NT_NEWS_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ROLES
--------------------------------------------------------

  ALTER TABLE "VITALYA"."ROLES" ADD CONSTRAINT "ROLES_PK" PRIMARY KEY ("RL_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "VITALYA"."ROLES" MODIFY ("RL_ID" NOT NULL ENABLE);
  ALTER TABLE "VITALYA"."ROLES" MODIFY ("RL_ROLE_NAME" NOT NULL ENABLE);
  ALTER TABLE "VITALYA"."ROLES" MODIFY ("RL_USER_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TAGS
--------------------------------------------------------

  ALTER TABLE "VITALYA"."TAGS" ADD CONSTRAINT "TAG_PK" PRIMARY KEY ("TAG_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "VITALYA"."TAGS" MODIFY ("TAG_NAME" NOT NULL ENABLE);
  ALTER TABLE "VITALYA"."TAGS" MODIFY ("TAG_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table USERS
--------------------------------------------------------

  ALTER TABLE "VITALYA"."USERS" ADD CONSTRAINT "LOGIN_UQ" UNIQUE ("USR_LOGIN")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "VITALYA"."USERS" ADD CONSTRAINT "USR_PK" PRIMARY KEY ("USR_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "VITALYA"."USERS" MODIFY ("USR_PASSWORD" NOT NULL ENABLE);
  ALTER TABLE "VITALYA"."USERS" MODIFY ("USR_LOGIN" NOT NULL ENABLE);
  ALTER TABLE "VITALYA"."USERS" MODIFY ("USR_NAME" NOT NULL ENABLE);
  ALTER TABLE "VITALYA"."USERS" MODIFY ("USR_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table COMMENTS
--------------------------------------------------------

  ALTER TABLE "VITALYA"."COMMENTS" ADD CONSTRAINT "CMT_NWS_FK" FOREIGN KEY ("CMT_NEWS_ID")
	  REFERENCES "VITALYA"."NEWS" ("NEWS_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NEWS_AUTHORS
--------------------------------------------------------

  ALTER TABLE "VITALYA"."NEWS_AUTHORS" ADD CONSTRAINT "NA_AUTH_FK" FOREIGN KEY ("NA_AUTHOR_ID")
	  REFERENCES "VITALYA"."AUTHORS" ("AUTH_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NEWS_TAGS
--------------------------------------------------------

  ALTER TABLE "VITALYA"."NEWS_TAGS" ADD CONSTRAINT "NT_NWS_FK" FOREIGN KEY ("NT_NEWS_ID")
	  REFERENCES "VITALYA"."NEWS" ("NEWS_ID") ENABLE;
  ALTER TABLE "VITALYA"."NEWS_TAGS" ADD CONSTRAINT "NT_TAG_FK" FOREIGN KEY ("NT_TAG_ID")
	  REFERENCES "VITALYA"."TAGS" ("TAG_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ROLES
--------------------------------------------------------

  ALTER TABLE "VITALYA"."ROLES" ADD CONSTRAINT "RL_USR_FK" FOREIGN KEY ("RL_USER_ID")
	  REFERENCES "VITALYA"."USERS" ("USR_ID") ENABLE;
--------------------------------------------------------
--  DDL for Trigger AUTHOR_T1
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "VITALYA"."AUTHOR_T1" 
BEFORE INSERT ON AUTHORS
FOR EACH ROW
BEGIN
 SELECT AUTH_PK_SEQ.NEXTVAL
  INTO   :new.AUTH_ID
  FROM   dual;
END;
/
ALTER TRIGGER "VITALYA"."AUTHOR_T1" ENABLE;
--------------------------------------------------------
--  DDL for Trigger COMMENTS_T1
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "VITALYA"."COMMENTS_T1" 
BEFORE INSERT ON COMMENTS 
FOR EACH ROW
BEGIN
  SELECT COM_PK_SEQ.NEXTVAL
  INTO   :new.CMT_ID
  FROM   dual;
END;
/
ALTER TRIGGER "VITALYA"."COMMENTS_T1" ENABLE;
--------------------------------------------------------
--  DDL for Trigger NEWS_T1
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "VITALYA"."NEWS_T1" 
BEFORE INSERT ON NEWS
FOR EACH ROW
BEGIN
  SELECT NEWS_PK_SEQ.NEXTVAL
  INTO   :new.NEWS_ID
  FROM   dual;
END;
/
ALTER TRIGGER "VITALYA"."NEWS_T1" ENABLE;
--------------------------------------------------------
--  DDL for Trigger ROLE_T1
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "VITALYA"."ROLE_T1" 
BEFORE INSERT ON ROLES
FOR EACH ROW
BEGIN
  SELECT RL_PK_SEQ.NEXTVAL
  INTO   :new.RL_ID
  FROM   dual;
END;
/
ALTER TRIGGER "VITALYA"."ROLE_T1" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TAG_T1
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "VITALYA"."TAG_T1" 
BEFORE INSERT ON TAGS 
FOR EACH ROW
BEGIN
  SELECT TAG_PK_SEQ.NEXTVAL
  INTO   :new.TAG_ID
  FROM   dual;
END;
/
ALTER TRIGGER "VITALYA"."TAG_T1" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USER_T1
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "VITALYA"."USER_T1" 
BEFORE INSERT ON USERS 
FOR EACH ROW
BEGIN
  SELECT USR_PK_SEQ.NEXTVAL
  INTO   :new.USR_ID
  FROM   dual;
END;
/
ALTER TRIGGER "VITALYA"."USER_T1" ENABLE;
