package com.epam.newsmanagement.service;

import static org.mockito.Mockito.times;

import java.sql.Date;
import java.sql.Timestamp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsServiceImpl;

@ContextConfiguration(locations = { "classpath:/context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)

public class NewsServiceImplTest {
	@Mock
	private AuthorDAO authorDao;
	@Mock
	private NewsDAO newsDao;
	@Mock
	private CommentDAO commentsDao;
	@Mock
	private TagDAO tagDao;
	@InjectMocks
	private NewsServiceImpl service;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void addNewsTest() throws ServiceException, DAOException {
		News news = new News(1L, "Title", "Shorttext", "longtext", new Timestamp(System.currentTimeMillis()),
				new Date(System.currentTimeMillis()));
		Mockito.when(newsDao.add(news)).thenReturn(1L);
		service.addNews(news);
		Mockito.verify(newsDao, times(1)).add(news);
	}

	@Test
	public void removeNewsTest() throws ServiceException, DAOException {
		long newsId = 1L;
		service.removeNews(newsId);
		Mockito.verify(newsDao, times(1)).removeById(newsId);
	}

	@Test
	public void updateNewsAuthorTest() throws ServiceException, DAOException {
		long newsId = 1L;
		long authorId = 2L;
		service.updateNewsAuthor(newsId, authorId);
		Mockito.verify(newsDao, times(1)).removeAuthorRelationByNewsId(newsId);
		Mockito.verify(newsDao, times(1)).addAuthorToNews(newsId, authorId);
	}
}
