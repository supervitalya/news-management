package com.epam.newsmanagement.dao.hibernate;

import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.sql.Timestamp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DAOException;

@ContextConfiguration(locations = { "classpath:/context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsDAOHiberTest {

	@Autowired
	@Qualifier("newsDaoHiber")
	NewsDAO newsDao;

	@Autowired
	@Qualifier("authorDaoHiber")
	AuthorDAO authorDao;

	@Test
	public void addNewsTest() throws DAOException {
		News news = new News();
		long current = System.currentTimeMillis();
		news.setCreationDate(new Timestamp(current));
		news.setFullText("YoyoyoasdasdasdasdasFullText5.11.16");
		news.setModificationDate(new Date(current));
		news.setShortText("YoyoyoffrtText5.11.16");
		news.setTitle("yoyofftle51116");

		Author author = new Author("Yoyoyoy");
		long authorId = authorDao.add(author);
		assertTrue(authorId != 0);
		System.out.println("authorId: " + authorId);
		long newsId = newsDao.add(news);
		assertTrue(newsId != 0);
		System.out.println("newsId: " + newsId);
		newsDao.addAuthorToNews(newsId, authorId);
	}
}
