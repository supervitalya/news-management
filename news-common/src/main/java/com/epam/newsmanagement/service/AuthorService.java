package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Provides methods for actions with Author entities.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
public interface AuthorService {
	/**
	 * Adds Author entity. If Author with such name already exist returns false.
	 * 
	 * @param author
	 *            entity
	 * @return Author if successful
	 * @throws ServiceException
	 */
	Author addAuthor(Author author) throws ServiceException;

	/**
	 * Updates specified Author entity with specified id and makes it expired.
	 * 
	 * @param author
	 *            entity
	 * @return true if author is found and updating is successful. Returns false
	 *         if author is not found.
	 * @throws ServiceException
	 */
	void setAuthorExpired(Long authorId) throws ServiceException;

	/**
	 * Fetches Author entity with specified id.
	 * 
	 * @param authorId
	 *            author entity id
	 * @return author entity
	 * @throws ServiceException
	 */
	Author fetchAuthor(Long authorId) throws ServiceException;

	/**
	 * Fetches Author entities list.
	 * 
	 * @return list of Author entity
	 * @throws ServiceException
	 */
	List<Author> fetchAllAuthors() throws ServiceException;

	/**
	 * Fetches Author entities list which are not expired.
	 * 
	 * @return list of Author entity
	 * @throws ServiceException
	 */
	List<Author> fetchNotExpiredAuthors() throws ServiceException;

	/**
	 * Fetches Author entity by specified id of News entity.
	 * 
	 * @param newsId
	 *            News entity id
	 * @return author entity
	 * @throws ServiceException
	 */
	Author fetchByNewsId(Long newsId) throws ServiceException;

	/**
	 * Updates Author entity.
	 * 
	 * @param author
	 *            entity should be updated
	 * @throws ServiceException
	 */
	void update(Author author) throws ServiceException;

	/**
	 * Removes Author entity
	 * 
	 * @param authorId
	 *            entity id should be removed.
	 * @throws ServiceException
	 */
	void remove(Long authorId) throws ServiceException;
}
