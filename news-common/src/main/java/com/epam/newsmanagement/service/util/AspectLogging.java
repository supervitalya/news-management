package com.epam.newsmanagement.service.util;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class AspectLogging {

	private static final Logger LOG = Logger.getLogger(AspectLogging.class);

	@Before("execution(* com.epam.newsmanagement.service.*.*(..))")
	public void logBefore(JoinPoint joinPoint) {
		LOG.info("Method " + joinPoint.getSignature().getName() + " is started.");
	}

	@AfterReturning(pointcut = "execution(* com.epam.newsmanagement.service.*.*(..))")
	public void doAccessCheck(JoinPoint joinPoint) {
		LOG.info("Method " + joinPoint.getSignature().getName() + " is finished.");
	}

}
