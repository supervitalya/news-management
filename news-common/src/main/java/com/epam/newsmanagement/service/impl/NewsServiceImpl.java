package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.criteria.SearchCriteria;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.NoSuchEntityException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;

@Service("newsService")
public class NewsServiceImpl implements NewsService {
	@Autowired
	@Qualifier("newsDao")
	private NewsDAO newsDao;

	@Override
	public Long addNews(News news) throws ServiceException {
		Long newsId = null;
		try {
			newsId = newsDao.add(news);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return newsId;
	}

	@Override
	public void removeNews(Long newsId) throws ServiceException {
		try {
			newsDao.removeById(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void editNews(News news) throws ServiceException {
		try {
			newsDao.update(news);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<News> searchByCriteria(SearchCriteria sc) throws ServiceException {
		try {
			return newsDao.fetchByCriteria(sc);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public News fetchNews(Long newsId) throws ServiceException {
		News news = null;
		try {
			news = newsDao.fetchById(newsId);
			if (news == null) {
				throw new NoSuchEntityException("News with id=" + newsId + " not found");
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return news;
	}

	@Override
	public void updateNewsAuthor(Long newsId, Long authorId) throws ServiceException {
		try {
			newsDao.removeAuthorRelationByNewsId(newsId);
			newsDao.addAuthorToNews(newsId, authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void removeAllTagsFromNews(Long newsId) throws ServiceException {
		try {
			newsDao.removeTagRelationsByNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void addTagsToNews(Long newsId, List<Long> tagsList) throws ServiceException {
		try {
			newsDao.addTagsToNews(newsId, tagsList);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void removeAuthorFromNews(Long newsId) throws ServiceException {
		try {
			newsDao.removeAuthorRelationByNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void addAuthorToNews(Long newsId, Long authorId) throws ServiceException {
		try {
			newsDao.addAuthorToNews(newsId, authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void updateTags(Long newsId, List<Long> tagsIdList) throws ServiceException {
		try {
			newsDao.removeTagRelationsByNewsId(newsId);
			newsDao.addTagsToNews(newsId, tagsIdList);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public Long fetchPreviousNewsId(Long newsId) throws ServiceException {
		try {
			return newsDao.fetchPreviousNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Long fetchNextNewsId(Long newsId) throws ServiceException {
		try {
			return newsDao.fetchNextNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Integer fetchByCriteriaAmount(SearchCriteria sc) throws ServiceException {
		Integer newsAmount = 0;
		try {
			newsAmount = newsDao.fetchByCriteriaAmount(sc);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return newsAmount;
	}
}
