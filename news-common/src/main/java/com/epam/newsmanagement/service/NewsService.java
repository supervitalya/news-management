package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.dao.criteria.SearchCriteria;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.NewsDTO;
import com.epam.newsmanagement.exception.ServiceException;

public interface NewsService {
	/**
	 * Adds News entity which.
	 * 
	 * @param news
	 *            transfer object
	 * @throws ServiceException
	 */
	Long addNews(News news) throws ServiceException;

	/**
	 * Updates News entity.
	 * 
	 * @param news
	 *            entity
	 * @throws ServiceException
	 */
	void editNews(News news) throws ServiceException;

	/**
	 * Removes News entity with specified id.
	 * 
	 * @param newsId
	 *            news entity id.
	 * @throws ServiceException
	 */
	void removeNews(Long newsId) throws ServiceException;

	/**
	 * Searches list of news according to SearchCriteria object. If there's no
	 * such News entity objects returns empty list.
	 * 
	 * @see SearchCriteria
	 * @param sc
	 *            search criteria object
	 * @return list of News entities
	 * @throws ServiceException
	 */
	List<News> searchByCriteria(SearchCriteria sc) throws ServiceException;

	/**
	 * Returns News transfer object which contains News entity, list of tags,
	 * list of comments and Author entity related to it.
	 * 
	 * @see NewsDTO
	 * @param newsId
	 *            News entity id
	 * @return news transfer object
	 * @throws ServiceException
	 */
	News fetchNews(Long newsId) throws ServiceException;

	/**
	 * Updates connection between News entity with specified id and Author
	 * entity with specified id.
	 * 
	 * @param newsId
	 *            news entity id
	 * @param authorId
	 *            author entity id
	 * @throws ServiceException
	 */
	void updateNewsAuthor(Long newsId, Long authorId) throws ServiceException;

	/**
	 * Adds connections between specified list of tags and News entity with
	 * specified id.
	 * 
	 * @param newsId
	 *            News entity id
	 * @param tagsList
	 *            list of Tag entities
	 * @throws ServiceException
	 */
	void addTagsToNews(Long newsId, List<Long> tagsIdList) throws ServiceException;

	/**
	 * Remove connections between all tags and News entity with specified id.
	 * 
	 * @param newsId
	 *            News entity id.
	 * @throws ServiceException
	 */
	void removeAllTagsFromNews(Long newsId) throws ServiceException;

	/**
	 * Remove connections between Author entity and News entity with specified
	 * id.
	 * 
	 * @param newsId
	 *            News entity id.
	 * @throws ServiceException
	 */
	void removeAuthorFromNews(Long newsId) throws ServiceException;

	/**
	 * Adds connection between News entity and it's Author
	 * 
	 * @param newsId
	 *            News entity id
	 * @param authorId
	 *            Author entity id
	 * @throws ServiceException
	 */
	void addAuthorToNews(Long newsId, Long authorId) throws ServiceException;

	void updateTags(Long newsId, List<Long> tagsIdList) throws ServiceException;

	Long fetchPreviousNewsId(Long newsId) throws ServiceException;

	Long fetchNextNewsId(Long newsId) throws ServiceException;

	Integer fetchByCriteriaAmount(SearchCriteria sc) throws ServiceException;
}
