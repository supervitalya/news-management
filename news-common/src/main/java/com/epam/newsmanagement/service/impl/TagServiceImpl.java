package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;

@Service("tagService")
public class TagServiceImpl implements TagService {
	@Autowired
	@Qualifier("tagsDao")
	private TagDAO tagDao;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Tag addTag(Tag tag) throws ServiceException {
		try {
			String tagName = tag.getTagName();
			if (tagDao.fetchByName(tagName) != null) {
				throw new ServiceException("Tag with specified name already exist");
			}
			Long tagId = tagDao.add(tag);
			tag.setId(tagId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return tag;
	}

	@Override
	public void removeTag(Long tagId) throws ServiceException {
		try {
			tagDao.removeById(tagId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Tag> fetchAllTags() throws ServiceException {
		try {
			return tagDao.fetchAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Tag> fetchByNewsId(Long newsId) throws ServiceException {
		try {
			return tagDao.fetchByNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void updateTag(Tag tag) throws ServiceException {
		try {
			tagDao.update(tag);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

}
