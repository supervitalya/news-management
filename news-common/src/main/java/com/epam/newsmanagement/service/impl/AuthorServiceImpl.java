package com.epam.newsmanagement.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.NoSuchEntityException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;

@Service("authorService")
public class AuthorServiceImpl implements AuthorService {
	@Autowired
	@Qualifier("authorDao")
	private AuthorDAO authorDao;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Author addAuthor(Author author) throws ServiceException {
		try {
			Long authorId = authorDao.add(author);
			author.setId(authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return author;
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void setAuthorExpired(Long authorId) throws ServiceException {
		try {
			Author newAuthor = authorDao.fetchById(authorId);
			if (newAuthor == null) {
				throw new NoSuchEntityException("Author not found");
			}
			newAuthor.setExpired(new Timestamp(System.currentTimeMillis()));
			authorDao.update(newAuthor);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Author fetchAuthor(Long authorId) throws ServiceException {
		Author author = null;
		try {
			author = authorDao.fetchById(authorId);
			if (author == null) {
				throw new NoSuchEntityException("Author not found");
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return author;
	}

	@Override
	public List<Author> fetchAllAuthors() throws ServiceException {
		List<Author> authorsList = new ArrayList<>();
		try {
			authorsList = authorDao.fetchAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return authorsList;
	}

	@Override
	public List<Author> fetchNotExpiredAuthors() throws ServiceException {
		List<Author> authorsList = new ArrayList<>();
		try {
			authorsList = authorDao.fetchNotExpired();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return authorsList;
	}

	@Override
	public Author fetchByNewsId(Long newsId) throws ServiceException {
		Author author;
		try {
			author = authorDao.fetchByNewsId(newsId);
			if (author == null) {
				throw new NoSuchEntityException("Author of news with newsId=" + newsId + " not found");
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return author;
	}

	@Override
	public void update(Author author) throws ServiceException {
		try {
			authorDao.update(author);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void remove(Long authorId) throws ServiceException {
		try {
			authorDao.removeById(authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}
}
