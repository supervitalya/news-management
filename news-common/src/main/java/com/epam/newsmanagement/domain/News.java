package com.epam.newsmanagement.domain;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.eclipse.persistence.annotations.ReturnInsert;

@Entity
@Table(name = "NEWS")
public class News {
	@Id
	@Column(name = "NEWS_ID", unique = true, nullable = false, updatable = false)
	@ReturnInsert(returnOnly = true)
	private Long id;
	@Pattern(regexp = "[(\\s){1}0-9A-Za-z\\u0410-\\u042F\\u0430-\\u044F]{3,50}")
	@Size(min = 3, max = 50)
	@Column(name = "NEWS_TITLE")
	private String title;
	@Size(min = 5, max = 100)
	@Column(name = "NEWS_SHORT_TEXT")
	private String shortText;
	@Size(min = 20, max = 2000)
	@Column(name = "NEWS_FULL_TEXT")
	private String fullText;
	@Column(name = "NEWS_CREATION_DATE")
	private Timestamp creationDate;
	@Column(name = "NEWS_MODIFICATION_DATE")
	private Date modificationDate;

	/*
	 * For hibernate
	 */
	@ManyToOne
	@JoinTable(name = "NEWS_AUTHORS", joinColumns = @JoinColumn(name = "NA_NEWS_ID"), inverseJoinColumns = @JoinColumn(name = "NA_AUTHOR_ID"))
	private Author author;
	@OneToMany
	private Set<Comment> comments;
	@ManyToMany
	@JoinTable(name = "NEWS_TAGS", joinColumns = @JoinColumn(name = "NT_NEWS_ID"), inverseJoinColumns = @JoinColumn(name = "NT_TAG_ID"))
	private Set<Tag> tags;

	public News() {
		super();
	}

	public News(String title, String shortText, String fullText, Timestamp creationDate, Date modificationDate) {
		super();
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	public News(Long id, String title, String shortText, String fullText, Timestamp creationDate) {
		super();
		this.id = id;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
	}

	public News(Long id, String title, String shortText, String fullText, Timestamp creationDate,
			Date modificationDate) {
		super();
		this.id = id;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "News [id=" + id + ", title=" + title + ", shortText=" + shortText + ", fullText=" + fullText
				+ ", creationDate=" + creationDate + ", modificationDate=" + modificationDate + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

}
