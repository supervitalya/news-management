package com.epam.newsmanagement.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.ReturnInsert;

@Entity
@Table(name = "COMMENTS")
public class Comment {

	@Id
	@Column(name = "CMT_ID")
	@ReturnInsert(returnOnly = true)
	private Long id;
	@Column(name = "CMT_NEWS_ID")
	private Long newsId;
	@Column(name = "CMT_TEXT")
	private String commentText;
	@Column(name = "CMT_CREATION_DATE")
	private Timestamp creationDate;

	/*
	 * For hibernate
	 */
	@ManyToOne
	@JoinColumn(name = "CMT_NEWS_ID", updatable = false, insertable = false)
	private News news;

	public Comment() {
		super();
	}

	public Comment(Long id, String commentText, Timestamp creationDate) {
		this.newsId = id;
		this.commentText = commentText;
		this.creationDate = creationDate;
	}

	public Comment(Long id, Long newsId, String commentText, Timestamp creationDate) {
		super();
		this.id = id;
		this.newsId = newsId;
		this.commentText = commentText;
		this.creationDate = creationDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", newsId=" + newsId + ", commentText=" + commentText + ", creationDate="
				+ creationDate + "]";
	}

}
