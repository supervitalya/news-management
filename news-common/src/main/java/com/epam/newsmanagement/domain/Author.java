package com.epam.newsmanagement.domain;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.eclipse.persistence.annotations.ReturnInsert;

@Entity
@Table(name = "AUTHORS")
public class Author {
	@Id
	@Column(name = "AUTH_ID")
	@ReturnInsert(returnOnly = true)
	private Long id;
	@Size(min = 2, max = 30)
	@Pattern(regexp = "[(\\s){1}0-9A-Za-z\\u0410-\\u042F\\u0430-\\u044F]{2,30}")
	@Column(name = "AUTH_NAME")
	private String authorName;
	@Column(name = "AUTH_EXPIRED")
	private Timestamp expired;
	/*
	 * For hibernate
	 */
	@OneToMany(mappedBy = "author")
	private Set<News> news;

	public Author() {
		super();
	}

	public Author(String authorName) {
		super();
		this.authorName = authorName;
	}

	public Author(Long id, String authorName) {
		super();
		this.id = id;
		this.authorName = authorName;
	}

	public Author(Long id, String authorName, Timestamp expired) {
		super();
		this.id = id;
		this.authorName = authorName;
		this.expired = expired;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public Timestamp getExpired() {
		return expired;
	}

	public void setExpired(Timestamp expired) {
		this.expired = expired;
	}

	public Set<News> getNews() {
		return news;
	}

	public void setNews(Set<News> news) {
		this.news = news;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authorName == null) ? 0 : authorName.hashCode());
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (authorName == null) {
			if (other.authorName != null)
				return false;
		} else if (!authorName.equals(other.authorName))
			return false;
		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Author [id=" + id + ", authorName=" + authorName + ", expired=" + expired + "]";
	}

}
