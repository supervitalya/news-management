package com.epam.newsmanagement.domain;

public class Role {
	private Long id;
	private Long userId;
	private String roleName;

	public Role() {
		super();
	}

	public Role(Long userId, String roleName) {
		super();
		this.userId = userId;
		this.roleName = roleName;
	}

	public Role(Long id, Long userId, String roleName) {
		super();
		this.id = id;
		this.userId = userId;
		this.roleName = roleName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", userId=" + userId + ", roleName=" + roleName + "]";
	}

}
