package com.epam.newsmanagement.domain;

import java.util.List;

import javax.validation.Valid;

public class NewsDTO {
	@Valid
	private News news;
	@Valid
	private Author author;
	private List<Tag> tagsList;
	private List<Comment> commentsList;

	public NewsDTO() {
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Tag> getTagsList() {
		return tagsList;
	}

	public void setTagsList(List<Tag> tagsList) {
		this.tagsList = tagsList;
	}

	public List<Comment> getCommentsList() {
		return commentsList;
	}

	public void setCommentsList(List<Comment> commentsList) {
		this.commentsList = commentsList;
	}

}
