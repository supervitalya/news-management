package com.epam.newsmanagement.domain;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.eclipse.persistence.annotations.ReturnInsert;

@Entity
@Table(name = "TAGS")
public class Tag {
	@Id
	@ReturnInsert(returnOnly = true)
	@Column(name = "TAG_ID")
	private Long id;
	@Size(min = 2, max = 30)
	@Pattern(regexp = "[(\\s){1}0-9A-Za-z\\u0410-\\u042F\\u0430-\\u044F]{2,30}")
	@Column(name = "TAG_NAME")
	private String tagName;
	/*
	 * For hibernate
	 */
	@ManyToMany
	@JoinTable(name = "NEWS_TAGS", joinColumns = @JoinColumn(name = "NT_TAG_ID"), inverseJoinColumns = @JoinColumn(name = "NT_NEWS_ID"))
	private Set<News> news;

	public Tag() {
		super();
	}

	public Tag(Long id) {
		super();
		this.id = id;
	}

	public Tag(Long id, String tagName) {
		super();
		this.id = id;
		this.tagName = tagName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public Set<News> getNews() {
		return news;
	}

	public void setNews(Set<News> news) {
		this.news = news;
	}

	@Override
	public String toString() {
		return "Tag [id=" + id + ", tagName=" + tagName + "]";
	}

}
