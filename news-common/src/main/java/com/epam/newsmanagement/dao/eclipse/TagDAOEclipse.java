package com.epam.newsmanagement.dao.eclipse;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.DAOException;

@SuppressWarnings("unchecked")
@Transactional(readOnly = false)
public class TagDAOEclipse implements TagDAO {
	private static final String PERSISTENCE_UNIT_NAME = "Eclipselink";

	@PersistenceContext
	EntityManager manager;

	@Override
	public Long add(Tag tag) throws DAOException {
		manager.persist(tag);
		manager.flush();
		return tag.getId();
	}

	@Override
	public List<Tag> fetchAll() throws DAOException {
		Query query = manager.createQuery("select t from Tag t order by t.tagName");
		List<Tag> tags = query.getResultList();
		return tags;
	}

	@Override
	public void removeById(Long tagId) throws DAOException {
		Query query = manager.createQuery("delete from Tag t where t.id=:tagId");
		query.setParameter("tagId", tagId);
		query.executeUpdate();
	}

	@Override
	public Tag fetchByName(String tagName) throws DAOException {
		Query query = manager.createQuery("select t from Tag t where t.tagName=:tagName");
		query.setParameter("tagName", tagName);
		Tag tag = null;
		List results = query.getResultList();
		if (!results.isEmpty()) {
			tag = (Tag) results.get(0);
		}
		return tag;
	}

	@Override
	public List<Tag> fetchByNewsId(Long newsId) throws DAOException {
		News news = manager.find(News.class, newsId);
		Set<Tag> tags = news.getTags();
		ArrayList<Tag> tagsList = new ArrayList<>(tags.size());
		tagsList.addAll(tags);
		return tagsList;
	}

	@Override
	public void update(Tag tag) throws DAOException {
		manager.merge(tag);
		manager.flush();
	}

}
