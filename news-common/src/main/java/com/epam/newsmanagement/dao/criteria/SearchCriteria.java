package com.epam.newsmanagement.dao.criteria;

import java.util.List;

/**
 * Provides criteria for News entities searching. Contains list of Author
 * entities and list of Tag entities. Depending on lists searching will perform.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
public class SearchCriteria {
	private long page;
	private long amount;
	private List<Long> authorsIdList;
	private List<Long> tagsIdList;
	private boolean isSortedByComments;

	public boolean isSortedByComments() {
		return isSortedByComments;
	}

	public void setSortedByComments(boolean isSortedByComments) {
		this.isSortedByComments = isSortedByComments;
	}

	public SearchCriteria() {
		super();
	}

	public List<Long> getAuthorsIdList() {
		return authorsIdList;
	}

	public void setAuthorsIdList(List<Long> authorsList) {
		this.authorsIdList = authorsList;
	}

	public List<Long> getTagsIdList() {
		return tagsIdList;
	}

	public void setTagsIdList(List<Long> tagList) {
		this.tagsIdList = tagList;
	}

	public long getPage() {
		return page;
	}

	public void setPage(long page) {
		this.page = page;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}
}
