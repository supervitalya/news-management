package com.epam.newsmanagement.dao.hibernate;

import java.util.HashSet;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.criteria.SearchCriteria;
import com.epam.newsmanagement.dao.criteria.SearchCriteriaBuilderJpa;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.DAOException;

@SuppressWarnings("unchecked")
@Transactional(readOnly = false)
public class NewsDAOHibernate extends HibernateDaoSupport implements NewsDAO {

	@Override
	public Long add(News news) throws DAOException {
		return (long) currentSession().save(news);
	}

	@Override
	public void removeById(Long newsId) throws DAOException {
		Session session = currentSession();
		News news = session.get(News.class, newsId);
		session.delete(news);
	}

	@Override
	public void update(News news) throws DAOException {
		currentSession().merge(news);
		currentSession().flush();
	}

	@Override
	public News fetchById(Long newsId) throws DAOException {
		News news = currentSession().get(News.class, newsId);
		currentSession().evict(news);
		return news;
	}

	@Override
	public void addAuthorToNews(Long newsId, Long authorId) throws DAOException {
		Session session = currentSession();
		Author author = session.get(Author.class, authorId);
		News news = session.get(News.class, newsId);
		news.setAuthor(author);
		session.persist(news);
	}

	@Override
	public void removeAuthorRelationByNewsId(Long newsId) throws DAOException {
	}

	@Override
	public void addTagsToNews(Long newsId, List<Long> tagsIdList) throws DAOException {
		Session session = currentSession();
		News news = session.get(News.class, newsId);
		Criteria tagCriteria = session.createCriteria(Tag.class);
		List<Tag> tags = tagCriteria.add(Restrictions.in("id", tagsIdList)).list();
		news.setTags(new HashSet<>(tags));
	}

	@Override
	public void removeTagRelationsByNewsId(Long newsId) throws DAOException {
	}

	@Override
	public List<News> fetchByCriteria(SearchCriteria sc) throws DAOException {
		int positionFrom = (int) ((sc.getPage() - 1) * sc.getAmount());
		int number = (int) sc.getAmount();

		String queryString = SearchCriteriaBuilderJpa.buildSearchQuery(sc);
		Query query = currentSession().createQuery(queryString);
		query.setFirstResult(positionFrom).setMaxResults(number);
		return query.list();
	}

	@Override
	public Long fetchPreviousNewsId(Long newsId) throws DAOException {
		News news = null;
		Criteria criteria = currentSession().createCriteria(News.class).addOrder(Order.desc("id"));
		ScrollableResults results = criteria.add(Restrictions.gt("id", newsId)).scroll();
		if (results.last()) {
			news = (News) results.get(0);
		}
		return news == null ? 0 : news.getId();
	}

	@Override
	public Long fetchNextNewsId(Long newsId) throws DAOException {
		News news = null;
		Criteria criteria = currentSession().createCriteria(News.class).addOrder(Order.desc("id"));
		Object newsObject = criteria.add(Restrictions.lt("id", newsId)).setMaxResults(1).uniqueResult();
		if ((newsObject != null) && (newsObject.getClass().equals(News.class))) {
			news = (News) newsObject;
		}
		return news == null ? 0 : news.getId();
	}

	@Override
	public Integer fetchByCriteriaAmount(SearchCriteria sc) throws DAOException {
		DetachedCriteria dc = readSearchCriteria(sc);
		Criteria fullCriteria = currentSession().createCriteria(News.class);
		fullCriteria.add(Subqueries.propertyIn("id", dc));

		Object o = fullCriteria.setProjection(Projections.rowCount()).uniqueResult();
		Long longValue = (Long) o;
		return longValue.intValue();
	}

	private DetachedCriteria readSearchCriteria(SearchCriteria sc) {
		DetachedCriteria dc = DetachedCriteria.forClass(News.class);
		dc.createAlias("tags", "t", JoinType.LEFT_OUTER_JOIN).createAlias("comments", "c", JoinType.LEFT_OUTER_JOIN)
				.createAlias("author", "auth", JoinType.LEFT_OUTER_JOIN);
		if (sc.getAuthorsIdList() != null) {
			dc.add(Restrictions.in("auth.id", sc.getAuthorsIdList()));
		}
		if (sc.getTagsIdList() != null) {
			dc.add(Restrictions.in("t.id", sc.getTagsIdList()));
		}
		dc.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		dc.setProjection(Projections.id());
		return dc;
	}

}
