package com.epam.newsmanagement.dao.eclipse;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DAOException;

@SuppressWarnings("unchecked")
@Transactional(readOnly = false)
public class AuthorDAOEclipse implements AuthorDAO {
	private static final String PERSISTENCE_UNIT_NAME = "Eclipselink";

	@PersistenceContext
	EntityManager manager;

	@Override
	public Long add(Author author) throws DAOException {
		manager.persist(author);
		return author.getId();
	}

	@Override
	public void update(Author author) throws DAOException {
		manager.merge(author);
		manager.flush();
	}

	@Override
	public List<Author> fetchAll() throws DAOException {
		Query query = manager.createQuery("select a from Author a order by a.authorName");
		List<Author> authors = query.getResultList();
		return authors;
	}

	@Override
	public void removeById(Long authorId) throws DAOException {
		Author author = manager.find(Author.class, authorId);
		manager.remove(author);
		manager.flush();
	}

	@Override
	public void updateNewsAuthor(Long newsId, Long authorId) throws DAOException {
	}

	@Override
	public Author fetchByName(String name) throws DAOException {
		Query query = manager.createQuery("select a from Author a where a.authorName=:name");
		query.setParameter(1, name);
		Author author = (Author) query.getResultList().get(0);
		return author;
	}

	@Override
	public Author fetchByNewsId(Long newsId) throws DAOException {
		News news = manager.find(News.class, newsId);
		Long authorId = news.getAuthor().getId();
		Author author = manager.find(Author.class, authorId);
		return author;
	}

	@Override
	public Author fetchById(Long authorId) throws DAOException {
		return manager.find(Author.class, authorId);
	}

	@Override
	public List<Author> fetchNotExpired() throws DAOException {
		Query query = manager.createQuery("select a from Author a where a.expired is null order by a.authorName");
		List<Author> authors = query.getResultList();
		return authors;
	}
}
