package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.criteria.SearchCriteria;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Provides access to News entities into database.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
public interface NewsDAO {
	/**
	 * Adds News entity.
	 * 
	 * @param News
	 *            entity.
	 * @throws DAOException
	 */

	Long add(News news) throws DAOException;

	/**
	 * Removes News entity with specified id.
	 * 
	 * @param News
	 *            entity id.
	 * @throws DAOException
	 */

	void removeById(Long newsId) throws DAOException;

	/**
	 * Updates News entity.
	 * 
	 * @param News
	 *            entity.
	 * @throws DAOException
	 */

	void update(News news) throws DAOException;

	/**
	 * Fetches News entity with specified id.
	 * 
	 * @param newsId
	 *            News entity id
	 * @return News entity
	 * @throws DAOException
	 */
	News fetchById(Long newsId) throws DAOException;

	/**
	 * Adds connection between News entity with specified id and Author entity
	 * with specified id.
	 * 
	 * @param newsId
	 *            News entity id
	 * @param authorId
	 *            Author entity id
	 * @throws DAOException
	 */
	void addAuthorToNews(Long newsId, Long authorId) throws DAOException;

	/**
	 * Removes connection between News entity with specified id and Author
	 * entity.
	 * 
	 * @param newsId
	 *            News entity id
	 * @throws DAOException
	 */
	void removeAuthorRelationByNewsId(Long newsId) throws DAOException;

	/**
	 * Adds connection between News entity with specified id and all Tag
	 * entities from the list.
	 * 
	 * @param newsId
	 *            News entity id
	 * @param tagsList
	 *            list of tags
	 * @throws DAOException
	 */
	void addTagsToNews(Long newsId, List<Long> tagsIdList) throws DAOException;

	/**
	 * Removes connections between News entity with such id and all tags.
	 * 
	 * @param newsId
	 *            news entity id
	 * @throws DAOException
	 */
	void removeTagRelationsByNewsId(Long newsId) throws DAOException;

	/**
	 * Fetches News entities selected by SearchCriteria object which contains
	 * list of tags and list of authors. If one of lists is empty or null it
	 * will not be include into searching query.
	 * 
	 * @param sc
	 *            SearchCriteria entity
	 * @return list of news entities
	 * @throws DAOException
	 */
	List<News> fetchByCriteria(SearchCriteria sc) throws DAOException;

	Long fetchPreviousNewsId(Long newsId) throws DAOException;

	Long fetchNextNewsId(Long newsId) throws DAOException;

	Integer fetchByCriteriaAmount(SearchCriteria sc) throws DAOException;

}
