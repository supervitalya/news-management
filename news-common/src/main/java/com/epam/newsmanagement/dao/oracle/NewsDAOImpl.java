package com.epam.newsmanagement.dao.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.criteria.SearchCriteria;
import com.epam.newsmanagement.dao.criteria.SearchCriteriaBuilder;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DAOException;

@Component
public class NewsDAOImpl implements NewsDAO {

	public static final String SQL_ADD_NEWS = "insert into NEWS (NEWS_TITLE,NEWS_SHORT_TEXT,"
			+ "NEWS_FULL_TEXT,NEWS_CREATION_DATE,NEWS_MODIFICATION_DATE) values(?,?,?,?,?)";

	public static final String SQL_REMOVE_NEWS_BY_ID = "delete from NEWS where NEWS_ID = ?";

	public static final String SQL_FETCH_NEWS_BY_ID = "select NEWS_TITLE,NEWS_SHORT_TEXT,"
			+ "NEWS_FULL_TEXT,NEWS_CREATION_DATE,NEWS_MODIFICATION_DATE from NEWS where NEWS_ID=?";

	public static final String SQL_UPDATE_NEWS = "update NEWS set NEWS_TITLE=?,NEWS_SHORT_TEXT=?,"
			+ "NEWS_FULL_TEXT=?,NEWS_MODIFICATION_DATE=? where NEWS_ID=?";

	public static final String SQL_COUNT_NEWS = "select count(NEWS_ID) from NEWS";

	public static final String SQL_ADD_AUTHOR_TO_NEWS = "insert into NEWS_AUTHORS (NA_NEWS_ID,"
			+ "NA_AUTHOR_ID) values(?,?)";

	public static final String SQL_REMOVE_AUTHOR_BY_NEWS_ID = "delete from NEWS_AUTHORS where NA_NEWS_ID=?";

	public static final String SQL_ADD_TAG_TO_NEWS = "insert into NEWS_TAGS (NT_NEWS_ID,NT_TAG_ID) values(?,?)";

	public static final String SQL_REMOVE_TAGS_BY_NEWS_ID = "delete from NEWS_TAGS where NT_NEWS_ID  = ?";

	public static final String SQL_PREVIOUS = "select NEWS_ID, lead(NEWS_ID,1,0) over (order by NEWS_ID desc) as prevn from NEWS";
	public static final String SQL_NEXT = "select NEWS_ID, lag(NEWS_ID,1,0) over (order by NEWS_ID desc) as nextn from NEWS";

	@Autowired
	private DataSource dataSource;

	@Override
	public Long add(News news) throws DAOException {
		Long newsId;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_ADD_NEWS, new int[] { 1 })) {
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setTimestamp(4, news.getCreationDate());
			ps.setDate(5, news.getModificationDate());
			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			newsId = rs.getLong(1);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return newsId;

	}

	@Override
	public void removeById(Long newsId) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_REMOVE_NEWS_BY_ID)) {
			ps.setLong(1, newsId);
			ps.execute();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public void update(News news) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_UPDATE_NEWS)) {
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setDate(4, news.getModificationDate());
			ps.setLong(5, news.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public void addAuthorToNews(Long newsId, Long authorId) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_ADD_AUTHOR_TO_NEWS)) {
			ps.setLong(1, newsId);
			ps.setLong(2, authorId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	@Override
	public void removeAuthorRelationByNewsId(Long newsId) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_REMOVE_AUTHOR_BY_NEWS_ID)) {
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	@Override
	public void addTagsToNews(Long newsId, List<Long> tagsIdList) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_ADD_TAG_TO_NEWS)) {
			for (Long tagId : tagsIdList) {
				ps.setLong(1, newsId);
				ps.setLong(2, tagId);
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public void removeTagRelationsByNewsId(Long newsId) throws DAOException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_REMOVE_TAGS_BY_NEWS_ID)) {
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public List<News> fetchByCriteria(SearchCriteria sc) throws DAOException {
		String searchQuery;
		List<News> newsList = new ArrayList<>();

		searchQuery = SearchCriteriaBuilder.buildSearchQuery(sc);
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(searchQuery)) {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				News news = new News(rs.getLong(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getTimestamp(5), rs.getDate(6));
				newsList.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return newsList;
	}

	@Override
	public News fetchById(Long newsId) throws DAOException {
		News news = null;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_FETCH_NEWS_BY_ID)) {
			ps.setLong(1, newsId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				news = new News(newsId, rs.getString(1), rs.getString(2), rs.getString(3), rs.getTimestamp(4),
						rs.getDate(5));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return news;
	}

	@Override
	public Long fetchPreviousNewsId(Long newsId) throws DAOException {
		Long prevId = 0L;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_PREVIOUS)) {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				if (rs.getLong(1) == newsId) {
					prevId = rs.getLong(2);
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return prevId;
	}

	@Override
	public Long fetchNextNewsId(Long newsId) throws DAOException {
		Long nextId = 0L;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_NEXT)) {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				if (rs.getLong(1) == newsId) {
					nextId = rs.getLong(2);
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return nextId;
	}

	@Override
	public Integer fetchByCriteriaAmount(SearchCriteria sc) throws DAOException {
		Integer newsAmount = 0;
		String searchQuery = SearchCriteriaBuilder.buildQueryForAmount(sc);
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(searchQuery)) {
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				newsAmount = rs.getInt(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return newsAmount;
	}

}
