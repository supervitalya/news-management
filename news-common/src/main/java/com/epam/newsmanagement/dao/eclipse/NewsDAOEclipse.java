package com.epam.newsmanagement.dao.eclipse;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.criteria.SearchCriteria;
import com.epam.newsmanagement.dao.criteria.SearchCriteriaBuilder;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.DAOException;

@SuppressWarnings("unchecked")
public class NewsDAOEclipse implements NewsDAO {
	private static final String PERSISTENCE_UNIT_NAME = "Eclipselink";

	@PersistenceContext
	EntityManager manager;

	@Override
	public Long add(News news) throws DAOException {
		manager.persist(news);
		manager.flush();
		return news.getId();
	}

	@Override
	public void removeById(Long newsId) throws DAOException {
		News news = manager.find(News.class, newsId);
		manager.remove(news);
	}

	@Override
	public void update(News news) throws DAOException {
		manager.merge(news);
		manager.flush();
	}

	@Override
	public News fetchById(Long newsId) throws DAOException {
		News news = manager.find(News.class, newsId);
		return news;
	}

	@Override
	public void addAuthorToNews(Long newsId, Long authorId) throws DAOException {
		News news = manager.find(News.class, newsId);
		Author author = manager.find(Author.class, authorId);
		news.setAuthor(author);
		manager.flush();
	}

	@Override
	public void removeAuthorRelationByNewsId(Long newsId) throws DAOException {
		return;
	}

	@Override
	public void addTagsToNews(Long newsId, List<Long> tagsIdList) throws DAOException {
		News news = manager.find(News.class, newsId);
		Set<Tag> tags = tagsIdList.stream().map(id -> manager.find(Tag.class, id)).collect(Collectors.toSet());
		news.setTags(tags);
		manager.flush();
	}

	@Override
	public void removeTagRelationsByNewsId(Long newsId) throws DAOException {
		return;
	}

	@Override
	public List<News> fetchByCriteria(SearchCriteria sc) throws DAOException {

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<News> criteriaQuery = builder.createQuery(News.class);
		criteriaQuery.distinct(true);
		Root<News> root = criteriaQuery.from(News.class);
		Join<News, Author> newsAuthor = root.join("author", JoinType.LEFT);
		Join<News, Tag> newsTag = root.join("tags", JoinType.LEFT);
		Join<News, Comment> newsComment = root.join("comments", JoinType.LEFT);

		if (sc.getTagsIdList() != null) {
			sc.getTagsIdList().forEach(id -> criteriaQuery.where(builder.or(builder.equal(newsTag.get("id"), id))));
		}
		if (sc.getAuthorsIdList() != null) {
			sc.getAuthorsIdList()
					.forEach(id -> criteriaQuery.where(builder.or(builder.equal(newsAuthor.get("id"), id))));
		}
		if (sc.isSortedByComments()) { // criteriaQuery.groupBy(root.get("id"));
			// criteriaQuery.orderBy(builder.desc(builder.count(newsComment.get("id"))));

		} else {
			criteriaQuery.orderBy(builder.desc(root.get("id")));
		}

		TypedQuery<News> query = manager.createQuery(criteriaQuery);
		query.setFirstResult((int) ((sc.getPage() - 1) * sc.getAmount())).setMaxResults((int) sc.getAmount());
		List<News> news = query.getResultList();
		return news;
		/*
		 * Query query = manager.createQuery(
		 * "select distinct n from News n left join Author a left join Tag t left join Comment c order by(select count(c.id) from News n left join Comment nc group by c.newsId)"
		 * ); List<News> news = query.getResultList(); return news;
		 */
	}

	@Override
	public Long fetchPreviousNewsId(Long newsId) throws DAOException {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<News> criteriaQuery = builder.createQuery(News.class);
		criteriaQuery.distinct(true);
		Root<News> root = criteriaQuery.from(News.class);
		criteriaQuery.orderBy(builder.desc(root.get("id")));
		criteriaQuery.where(builder.lt(root.get("id"), newsId));
		TypedQuery<News> query = manager.createQuery(criteriaQuery);
		List<News> resultList = query.getResultList();
		Long previousId = 0L;
		if ((resultList != null) && (!resultList.isEmpty())) {
			previousId = resultList.get(0).getId();
		}
		return previousId;
	}

	@Override
	public Long fetchNextNewsId(Long newsId) throws DAOException {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<News> criteriaQuery = builder.createQuery(News.class);
		criteriaQuery.distinct(true);
		Root<News> root = criteriaQuery.from(News.class);
		criteriaQuery.orderBy(builder.desc(root.get("id")));
		criteriaQuery.where(builder.gt(root.get("id"), newsId));
		TypedQuery<News> query = manager.createQuery(criteriaQuery);
		List<News> resultList = query.getResultList();
		Long nextId = 0L;
		if ((resultList != null) && (!resultList.isEmpty())) {
			nextId = resultList.get(resultList.size() - 1).getId();
		}
		return nextId;
	}

	@Override
	public Integer fetchByCriteriaAmount(SearchCriteria sc) throws DAOException {
		String nativeQuery = SearchCriteriaBuilder.buildQueryForAmount(sc);
		Query query = manager.createNativeQuery(nativeQuery);
		BigDecimal result = (BigDecimal) query.getResultList().get(0);
		int number = result.intValue();
		return number;
	}

}
