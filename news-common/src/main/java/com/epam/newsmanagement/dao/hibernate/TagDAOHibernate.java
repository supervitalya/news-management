package com.epam.newsmanagement.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.DAOException;

@Transactional(readOnly = false)
@SuppressWarnings("unchecked")

public class TagDAOHibernate extends HibernateDaoSupport implements TagDAO {

	@Override
	public Long add(Tag tag) throws DAOException {
		return (long) currentSession().save(tag);
	}

	@Override
	public List<Tag> fetchAll() throws DAOException {
		Criteria criteria = currentSession().createCriteria(Tag.class).addOrder(Order.asc("tagName"));
		List<Tag> tags = criteria.list();
		return tags;
	}

	@Override
	public void removeById(Long tagId) throws DAOException {
		Session session = currentSession();
		Tag tag = session.get(Tag.class, tagId);
		session.delete(tag);
	}

	@Override
	public Tag fetchByName(String tagName) throws DAOException {
		List<Tag> tags = currentSession().createCriteria(Tag.class).add(Restrictions.like("tagName", tagName)).list();
		if (tags.isEmpty()) {
			return null;
		}
		return tags.get(0);
	}

	@Override
	public List<Tag> fetchByNewsId(Long newsId) throws DAOException {
		News news = currentSession().get(News.class, newsId);
		List<Tag> tags = new ArrayList<>(news.getTags());
		tags.sort((t1, t2) -> t1.getTagName().compareTo(t2.getTagName()));
		return tags;
	}

	@Override
	public void update(Tag tag) throws DAOException {
		currentSession().update(tag);
	}

}
