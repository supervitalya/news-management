package com.epam.newsmanagement.dao.criteria;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

/**
 * Utility class that provides method for generation query from SearchQuery
 * object.
 * 
 * @see SearchCriteria
 * @author Vitaly_Blyaharchuk
 *
 */
public class SearchCriteriaBuilder {
	private static final Logger log = Logger.getLogger(SearchCriteriaBuilder.class);
	public static final String BASIC_QUERY_BY_TAGS = "NEWS_TAGS.NT_TAG_ID in(";
	public static final String BASIC_QUERY_BY_AUTHORS = "NEWS_AUTHORS.NA_AUTHOR_ID in(";

	public static final String BASIC_QUERY_BY_CRITERIA = "select NEWS_ID,NEWS_TITLE,"
			+ "NEWS_SHORT_TEXT,NEWS_FULL_TEXT,NEWS_CREATION_DATE,NEWS_MODIFICATION_DATE "
			+ "from (select NEWS_ID,NEWS_TITLE,NEWS_SHORT_TEXT,NEWS_FULL_TEXT,"
			+ "NEWS_CREATION_DATE,NEWS_MODIFICATION_DATE,ROW_NUMBER() OVER(ORDER BY "
			+ "NEWS_ID DESC) RowNumber from (select distinct NEWS_ID,NEWS_TITLE,NEWS_SHORT_TEXT,"
			+ "NEWS_FULL_TEXT,NEWS_CREATION_DATE,NEWS_MODIFICATION_DATE from NEWS "
			+ "left join NEWS_AUTHORS on NEWS_ID = NA_NEWS_ID left join NEWS_TAGS on NEWS_ID = NT_NEWS_ID where ";

	public static final String BASIC_QUERY_END = ")) where RowNumber BETWEEN ";

	public static final String BASIC_QUERY_BY_CRITERIA_SORTED_BY_COMMENTS = "select NEWS_ID,"
			+ "NEWS_TITLE,NEWS_SHORT_TEXT,NEWS_FULL_TEXT,NEWS_CREATION_DATE,NEWS_MODIFICATION_DATE from (select "
			+ "NEWS_ID,NEWS_TITLE,NEWS_SHORT_TEXT,NEWS_FULL_TEXT,NEWS_CREATION_DATE,NEWS_MODIFICATION_DATE,"
			+ "ROW_NUMBER() OVER(ORDER BY (select count(1) from COMMENTS where CMT_NEWS_ID=NEWS_ID)"
			+ "DESC,NEWS_ID desc) RowNumber from (select distinct NEWS_ID,NEWS_TITLE,NEWS_SHORT_TEXT,"
			+ "NEWS_FULL_TEXT,NEWS_CREATION_DATE,NEWS_MODIFICATION_DATE from NEWS left join "
			+ "NEWS_AUTHORS on NEWS_ID = NA_NEWS_ID left join NEWS_TAGS on NEWS_ID = NT_NEWS_ID left "
			+ "join COMMENTS on NEWS_ID = CMT_NEWS_ID where ";
	public static final String BASIC_SORTED_QUERY_END = " order by(select count(1) from "
			+ "COMMENTS where CMT_NEWS_ID=NEWS_ID)DESC,NEWS_ID desc)) where RowNumber BETWEEN ";

	/**
	 * Returns query built for searching News entities using lists of Tag and
	 * Author entities connected to id.
	 * 
	 * @param sc
	 *            search criteria object
	 * @return searching query
	 */

	public static String buildSearchQuery(SearchCriteria sc) {
		long page = sc.getPage();
		long amount = sc.getAmount();
		boolean isSorted = sc.isSortedByComments();
		if ((page <= 0) || (amount <= 0)) {
			throw new IllegalArgumentException("Cannot fetch entities from page " + page + " with amount " + amount);
		}

		StringBuilder query = new StringBuilder();
		List<Long> authorsIdList = sc.getAuthorsIdList();
		List<Long> tagsIdList = sc.getTagsIdList();
		boolean authorAdded = false;
		if (isSorted) {
			query.append(BASIC_QUERY_BY_CRITERIA_SORTED_BY_COMMENTS);
		} else {
			query.append(BASIC_QUERY_BY_CRITERIA);
		}
		if (((sc.getAuthorsIdList() == null) || (sc.getAuthorsIdList().isEmpty()))
				&& (((sc.getTagsIdList() == null) || (sc.getTagsIdList().isEmpty())))) {
			query.delete(query.length() - 6, query.length());
		}

		if ((authorsIdList != null) && (!authorsIdList.isEmpty())) {
			query.append(buildAuthorsQuery(authorsIdList));
			authorAdded = true;
		}

		if ((tagsIdList != null) && (!tagsIdList.isEmpty())) {
			if (authorAdded) {
				query.append(" AND ");
			}
			query.append(buildTagsQuery(tagsIdList));
		}
		if (isSorted) {
			query.append(BASIC_SORTED_QUERY_END);
		} else {
			query.append(BASIC_QUERY_END);
		}
		query.append((page - 1) * amount + 1);
		query.append(" AND ");
		long endsWith = page * amount;
		query.append(endsWith);
		log.info("QUERY: " + query);
		return query.toString();
	}

	public static String buildQueryForAmount(SearchCriteria sc) {
		// Default search query with rowNum
		String searchQuery = buildSearchQuery(sc);
		String amountQuery;
		amountQuery = "select count(NEWS_ID) ";
		StringBuilder sb = new StringBuilder(amountQuery);
		// Selecting start index without any fields. We need only count() here.
		int startIndex = searchQuery.indexOf("from");
		int endIndex = searchQuery.lastIndexOf("RowNumber") - 7;
		amountQuery = searchQuery.substring(startIndex, endIndex);
		sb.append(amountQuery);
		return sb.toString();
	}

	private static String buildTagsQuery(List<Long> tagsIdList) {
		StringBuilder query = new StringBuilder(BASIC_QUERY_BY_TAGS);
		String joinedTagsIsd = tagsIdList.stream().map(String::valueOf).collect(Collectors.joining(","));
		query.append(joinedTagsIsd);
		query.append(")");
		return query.toString();
	}

	private static String buildAuthorsQuery(List<Long> authorsIdList) {
		StringBuilder query = new StringBuilder(BASIC_QUERY_BY_AUTHORS);
		String joinedAuthorsIds = authorsIdList.stream().map(String::valueOf).collect(Collectors.joining(","));
		query.append(joinedAuthorsIds);
		query.append(")");
		return query.toString();
	}
}
