package com.epam.newsmanagement.dao.criteria;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

/**
 * Utility class that provides method for generation query from SearchQuery
 * object.
 * 
 * @see SearchCriteria
 * @author Vitaly_Blyaharchuk
 *
 */
public class SearchCriteriaBuilderJpa {
	private static final Logger log = Logger.getLogger(SearchCriteriaBuilderJpa.class);
	public static final String BASIC_QUERY_BY_TAGS = "t.id in(";
	public static final String BASIC_QUERY_BY_AUTHORS = "a.id in(";

	public static final String BASIC_QUERY_BY_CRITERIA = "select distinct n from News n left join "
			+ "n.tags t left join n.author a where ";

	public static final String BASIC_QUERY_END = " order by n.creationDate desc";

	public static final String BASIC_QUERY_BY_CRITERIA_SORTED_BY_COMMENTS = "select count(c) as comments, n from "
			+ "News n left join n.tags t left join n.author a left join n.comments c where ";

	public static final String BASIC_SORTED_QUERY_END = " group by n order by comments desc";

	/**
	 * Returns query built for searching News entities using lists of Tag and
	 * Author entities.
	 * 
	 * @param sc
	 *            search criteria object
	 * @return search query
	 */

	public static String buildSearchQuery(SearchCriteria sc) {
		long page = sc.getPage();
		long amount = sc.getAmount();
		boolean isSorted = sc.isSortedByComments();
		if ((page <= 0) || (amount <= 0)) {
			throw new IllegalArgumentException("Cannot fetch entities from page " + page + " with amount " + amount);
		}

		StringBuilder query = new StringBuilder();
		List<Long> authorsIdList = sc.getAuthorsIdList();
		List<Long> tagsIdList = sc.getTagsIdList();
		boolean authorAdded = false;
		if (isSorted) {
			query.append(BASIC_QUERY_BY_CRITERIA_SORTED_BY_COMMENTS);
		} else {
			query.append(BASIC_QUERY_BY_CRITERIA);
		}
		if (((sc.getAuthorsIdList() == null) || (sc.getAuthorsIdList().isEmpty()))
				&& (((sc.getTagsIdList() == null) || (sc.getTagsIdList().isEmpty())))) {
			query.delete(query.length() - 6, query.length());
		}

		if ((authorsIdList != null) && (!authorsIdList.isEmpty())) {
			query.append(buildAuthorsQuery(authorsIdList));
			authorAdded = true;
		}

		if ((tagsIdList != null) && (!tagsIdList.isEmpty())) {
			if (authorAdded) {
				query.append(" AND ");
			}
			query.append(buildTagsQuery(tagsIdList));
		}
		if (isSorted) {
			query.append(BASIC_SORTED_QUERY_END);
		} else {
			query.append(BASIC_QUERY_END);
		}
		log.info("QUERY: " + query);
		return query.toString();
	}

	public static String buildQueryForAmount(SearchCriteria sc) {
		// Default search query with rowNum
		String searchQuery = buildSearchQuery(sc);
		String amountQuery;
		amountQuery = "select count(NEWS_ID) ";
		StringBuilder sb = new StringBuilder(amountQuery);
		// Selecting start index without any fields. We need only count() here.
		int startIndex = searchQuery.indexOf("from");
		int endIndex = searchQuery.lastIndexOf("RowNumber") - 7;
		amountQuery = searchQuery.substring(startIndex, endIndex);
		sb.append(amountQuery);
		return sb.toString();
	}

	private static String buildTagsQuery(List<Long> tagsIdList) {
		StringBuilder query = new StringBuilder(BASIC_QUERY_BY_TAGS);
		String joinedTagsIsd = tagsIdList.stream().map(String::valueOf).collect(Collectors.joining(","));
		query.append(joinedTagsIsd);
		query.append(")");
		return query.toString();
	}

	private static String buildAuthorsQuery(List<Long> authorsIdList) {
		StringBuilder query = new StringBuilder(BASIC_QUERY_BY_AUTHORS);
		String joinedAuthorsIds = authorsIdList.stream().map(String::valueOf).collect(Collectors.joining(","));
		query.append(joinedAuthorsIds);
		query.append(")");
		return query.toString();
	}
}
