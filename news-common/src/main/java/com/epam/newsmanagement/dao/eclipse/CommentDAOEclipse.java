package com.epam.newsmanagement.dao.eclipse;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.exception.DAOException;

@SuppressWarnings("unchecked")
@Transactional(readOnly = false)
public class CommentDAOEclipse implements CommentDAO {
	private static final String PERSISTENCE_UNIT_NAME = "Eclipselink";

	@PersistenceContext
	EntityManager manager;

	@Override
	public void removeById(Long commentId) throws DAOException {
		Query query = manager.createQuery("delete from Comment where id=:commentId");
		query.setParameter("commentId", commentId);
		query.executeUpdate();
	}

	@Override
	public Long add(Comment comment) throws DAOException {
		manager.persist(comment);
		return comment.getId();
	}

	@Override
	public void update(Comment comment) throws DAOException {
		manager.merge(comment);
		manager.flush();
	}

	@Override
	public void removeByNewsId(Long newsId) throws DAOException {
		Query query = manager.createQuery("delete from Comment c where c.newsId=:newsId");
		query.setParameter("newsId", newsId);
		query.executeUpdate();
	}

	@Override
	public List<Comment> fetchByNewsId(Long newsId) throws DAOException {
		Query query = manager.createQuery("select c from Comment c where c.newsId=:newsId order by c.id desc");
		query.setParameter("newsId", newsId);
		List<Comment> comments = query.getResultList();
		return comments;
	}

}
