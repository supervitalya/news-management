package com.epam.newsmanagement.dao.hibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DAOException;

@Transactional(readOnly = false)
@SuppressWarnings("unchecked")

public class CommentDAOHibernate extends HibernateDaoSupport implements CommentDAO {

	@Override
	public void removeById(Long commentId) throws DAOException {
		Session session = currentSession();
		Comment comment = session.get(Comment.class, commentId);
		session.delete(comment);
	}

	@Override
	public Long add(Comment comment) throws DAOException {
		return (long) currentSession().save(comment);
	}

	@Override
	public void update(Comment comment) throws DAOException {
		currentSession().update(comment);
	}

	@Override
	public void removeByNewsId(Long newsId) throws DAOException {
		News news = currentSession().get(News.class, newsId);
		news.getComments().stream().forEach(c -> currentSession().delete(c));
	}

	@Override
	public List<Comment> fetchByNewsId(Long newsId) throws DAOException {
		List<Comment> comments = currentSession().createCriteria(Comment.class).createAlias("news", "n")
				.add(Restrictions.eq("n.id", newsId)).addOrder(Order.desc("id")).list();
		return comments;
	}

}
