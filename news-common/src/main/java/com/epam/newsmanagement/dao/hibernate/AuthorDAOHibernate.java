package com.epam.newsmanagement.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DAOException;

@SuppressWarnings("unchecked")
@Transactional(readOnly = false)
public class AuthorDAOHibernate extends HibernateDaoSupport implements AuthorDAO {

	@Override
	public Long add(Author author) throws DAOException {
		return (long) currentSession().save(author);
	}

	@Override
	public void update(Author author) throws DAOException {
		currentSession().update(author);
	}

	@Override
	public List<Author> fetchAll() throws DAOException {
		List<Author> authors = currentSession().createCriteria(Author.class).addOrder(Order.asc("authorName")).list();
		return authors;
	}

	@Override
	public void removeById(Long authorId) throws DAOException {
		Query query = currentSession().createQuery("delete from Author where id=:authorId");
		query.setLong("authorId", authorId);
		query.executeUpdate();
	}

	@Override
	public void updateNewsAuthor(Long newsId, Long authorId) throws DAOException {
		Session session = currentSession();
		News news = session.get(News.class, newsId);
		Author author = session.get(Author.class, authorId);
		news.setAuthor(author);
	}

	@Override
	public Author fetchByName(String name) throws DAOException {
		List<Author> authors = currentSession().createCriteria(Author.class).add(Restrictions.like("authorName", name))
				.list();
		return authors.get(0);
	}

	@Override
	public Author fetchByNewsId(Long newsId) throws DAOException {
		News news = currentSession().get(News.class, newsId);
		return news.getAuthor();
	}

	@Override
	public Author fetchById(Long authorId) throws DAOException {
		return currentSession().get(Author.class, authorId);
	}

	@Override
	public List<Author> fetchNotExpired() throws DAOException {
		List<Author> authors = currentSession().createCriteria(Author.class).add(Restrictions.isNull("expired"))
				.addOrder(Order.asc("authorName")).list();
		return authors;
	}
}
