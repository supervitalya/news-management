/**
 * 
 */
function validateNewsUpdateForm() {
	var title = $("#title").val();
	var shortText = $("#shortText").val();
	var fullText = $("#fullText").val();
	$("#titleError").text("");
	$("#shortError").text("");
	$("#fullError").text("");
	var status = true;
	var pattern = /[0-9a-zА-я]+$/i;
	if (title.length < 3 || title.length > 50 || !pattern.test(title)) {
		$("#titleError").text("Title is incorrect");
		status = false;
	}
	if (shortText.length < 5 || shortText.length > 100) {
		$("#shortError").text(
				"Short text is incorrect. Size must be between 5 and 100.");
		status = false;
	}
	if (fullText.length < 20 || fullText.length > 2000) {
		$("#fullError").text(
				"Full text is incorrect. Size must be between 20 and 2000.");
		status = false;
	}
	return status;
}
