<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="style/style.css">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>News service</title>

<script src="resources/jquery/jquery-2.2.4.min.js"></script>
<link rel="stylesheet"
	href="resources/bootstrap-3.3.6-dist/css/bootstrap.min.css">
<script src="resources/bootstrap-3.3.6-dist/js/bootstrap.js"></script>
<script src="resources/js/validatorScript.js"></script>
</head>
<body>
	<div class="page">
		<tiles:insertAttribute name="header" />
		<div class="content">
			<tiles:insertAttribute name="sidebar" />
			<div class="article">
				<tiles:insertAttribute name="body" />
			</div>
		</div>
		<tiles:insertAttribute name="footer" />
	</div>
</body>
</html>