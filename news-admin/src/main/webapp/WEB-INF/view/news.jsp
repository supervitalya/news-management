<%@ page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<div class="newsBody">

	<div class="nextPrevLinks">
		<c:if test="${previousNewsId!=0}">
			<a href="singleNews?id=${previousNewsId}" class="btn btn-primary">Previous</a>
		</c:if>
		<c:if test="${nextNewsId!=0}">
			<a href="singleNews?id=${nextNewsId}" class="btn btn-primary">Next</a>
		</c:if>
	</div>
	<form:form method="POST" action="updateNews" modelAttribute="newsDTO"
		id="updateForm" onSubmit="return validateNewsUpdateForm()">
		<table>
			<tr>
				<td>Tags:</td>
				<td><c:forEach var="tag" items="${newsDTO.tagsList}">
						<span class="label label-success"><c:out
								value="${tag.tagName}" /></span>
					</c:forEach></td>
			</tr>
			<tr>
				<td>Author</td>
				<td><span class="label label-danger"><c:out
							value="${newsDTO.author.authorName}" /></span></td>
			</tr>
			<tr>
				<td>Creation Date:</td>
				<td><fmt:formatDate pattern="yyyy-MM-dd hh:mm a"
						value="${newsDTO.news.creationDate}" /></td>
			</tr>
			<tr>
				<td>Modification Date:</td>
				<td><fmt:formatDate pattern="yyyy-MM-dd"
						value="${newsDTO.news.modificationDate}" /></td>
			</tr>
			<tr>
				<td>Id:</td>
				<td><form:input path="news.id" size="10" readonly="true" /></td>
			</tr>
			<tr>
				<td>Title:</td>
				<!--  <td><form:input path="news.title" size="50"
					pattern="[(\s){1}0-9A-Za-z\u0410-\u042F\u0430-\u044F]{3,50}"
						id="title" /></td>-->
				<td><form:input path="news.title" size="67" id="title" /></td>
				<td><form:errors path="news.title" class="err" /></td>
				<td><p id="titleError" class="err"></p></td>
			</tr>

			<tr>
				<td>Short text:</td>
				<td><form:textarea path="news.shortText" rows="4" cols="70"
						pattern="(.){5,100}" id="shortText" /></td>
				<td><form:errors path="news.shortText" class="err" /></td>
				<td><p id="shortError" class="err"></p></td>

			</tr>
			<tr>
				<td>Full text:</td>
				<td><form:textarea path="news.fullText" rows="10" cols="70"
						pattern="(.){20,2000}" id="fullText" /></td>
				<td><form:errors path="news.fullText" class="err" /></td>
				<td><p id="fullError" class="err"></p></td>

			</tr>

			<tr>
				<td>Edit tags:</td>
				<td>Edit authors:</td>
			</tr>
			<tr>
				<td><select form="updateForm" name="tagsIdList" multiple>
						<c:forEach var="tag" items="${allTags}">
							<option value="${tag.id}">
								<c:out value="${tag.tagName}" />
							</option>
						</c:forEach>
				</select></td>

				<td><select form="updateForm" name="authorId">
						<c:forEach var="author" items="${allAuthors}">
							<option value="${author.id}">
								<c:out value="${author.authorName}" />
							</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<form:errors path="*" />
			</tr>
			<tr><td></td>
				<td><input type="submit" value="Save" class="btn btn-success saveButton" /></td>
			</tr>
		</table>
	</form:form>
	<div class="commentsList">
		<table>
			<c:forEach var="comment" items="${newsDTO.commentsList}">
				<tr>
					<td><fmt:formatDate pattern="yyyy-MM-dd"
							value="${comment.creationDate}" /></td>
					<td><c:out value="${comment.commentText}" /></td>
					<td><form method="POST" action="deleteComment">
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" /> <input type="hidden" name="commentId"
								value="${comment.id}"> <input type="hidden"
								name="newsId" value="${newsDTO.news.id}"> <input
								type="submit" value="Delete" class="btn btn-danger">
						</form></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</div>

