<%@ page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="body">
<div class="page-header">
	<h1>Add news</h1></div>
	<table>
		<form:form method="POST" modelAttribute="news" id="addForm"
			onSubmit="return validateNewsUpdateForm()">
			<tr>
				<td>Title:</td>
				<td><form:input path="news.title" size="67"
						pattern="[(\s){1}0-9A-Za-z\u0410-\u042F\u0430-\u044F]{3,50}"
						id="title" /></td>
				<td><form:errors path="news.title" class="err" /></td>
				<td><p id="titleError" class="err"></p></td>
			</tr>
			<tr>
				<td>Short text:</td>
				<td><form:textarea path="news.shortText" rows="4" cols="70"
						pattern=".{5,100}" id="shortText" /></td>
				<td><form:errors path="news.shortText" class="err" /></td>
				<td><p id="shortError" class="err"></p></td>

			</tr>
			<tr>
				<td>Full text:</td>
				<td><form:textarea path="news.fullText" rows="10" cols="70"
						pattern=".{10,2000}" id="fullText" /></td>
				<td><form:errors path="news.fullText" class="err" /></td>
				<td><p id="fullError" class="err"></p></td>
			</tr>
			<tr>
				<td>Tags:</td>
				<td><select form="addForm" name="tagsIdList" required multiple>
						<c:forEach var="tag" items="${allTags}">
							<option value="${tag.id}">
								<c:out value="${tag.tagName}" />
							</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td>Author:</td>
				<td><select form="addForm" name="authorId" required>
						<c:forEach var="author" items="${allAuthors}">
							<option value="${author.id}">
								<c:out value="${author.authorName}" />
							</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr><td></td>
				<td><input type="submit" value="Save" class="btn btn-success saveButton" /></td>
			</tr>

		</form:form>
	</table>
</div>
