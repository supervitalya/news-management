<%@ page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="body">
	<form:form method="GET" action="search" modelAttribute="searchCriteria"
		id="searchForm"><input type="hidden" name="specifySorting" value="true">
		<table>
			<tr>
				<td>Tags:</td>
			<td><form:select path="tagsIdList" multiple="true">
						<c:forEach var="tag" items="${allTags}">
							<option value="${tag.id}">
								<c:out value="${tag.tagName}" />
							</option>
						</c:forEach>
					</form:select></td>
				
				<td>Authors:</td>
				<td><form:select path="authorsIdList" multiple="true">
						<c:forEach var="author" items="${allAuthors}">
							<option value="${author.id}">
								<c:out value="${author.authorName}" />
							</option>
						</c:forEach>
					</form:select></td> 
				<td>Sort by comments:<c:choose>
						<c:when test="${sorted}">
							<form:checkbox path="sortedByComments" value="true"
								checked="true" />
						</c:when>
						<c:otherwise>
							<form:checkbox path="sortedByComments" value="true" />
						</c:otherwise>
					</c:choose></td>
				<td><input type="submit" value="Open" class="btn btn-success" />
				</td>
				<td></form:form> <form:form method="POST" action="resetSearch">
						<input type="submit" value="Reset" class="btn btn-danger">
					</form:form></td>
			</tr>
		</table>
		<hr />
		<c:choose>
			<c:when test="${not empty newsList}">
				<table>
					<c:forEach var="news" items="${newsList}">
					<tr>
						<td>Id: <c:out value="${news.id}" /></td>

						</tr>
						<tr>
							<td><span class="title"><c:out value="${news.title}" /></span></td>
						</tr>
						<tr>
							<td><span class="shortText"><c:out
										value="${news.shortText}" /></span></td>
						</tr>
						<tr>
							<td></td>
						<td><a href="singleNews?id=${news.id}"
								class="btn btn-success">Open</a></td>
							<td><a href="deleteNews?newsId=${news.id}"
								class="btn btn-danger">Delete</a></td>
						</tr>
					</c:forEach>
				</table>
			</c:when>
			<c:otherwise>
				<p>News list is empty.</p>
			</c:otherwise>
		</c:choose>
		<ul class="pagination">
			<c:forEach begin="1" end="${pagesAmount}" varStatus="loop">
				<li><a href="search?page=${loop.index}"> <c:out
							value="${loop.index}" /></a></li>
			</c:forEach>
		</ul>
</div>
