<%@ page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<script type="text/javascript">
	function showUpdate(i) {
		var element = document.getElementById("updateButton"+i);
		element.removeAttribute("disabled");
		element.className="btn btn-success";
	}
</script>
<div class="body">
	<div class="page-header">

		<h1>Tags list</h1>
	</div>
	<table>
		<tr>
			<th>Id</th>
			<th>Name</th>
		</tr>
		<c:forEach var="tag" items="${tagsList}">
			<tr>
				<td><c:out value="${tag.id}" /></td>
				<td><form:form method="POST" modelAttribute="tag"
						action="updateTag">
						<form:input type="text" path="tagName" value="${tag.tagName}"
							onChange="showUpdate(${tag.id})"
							pattern="[(\s){1}0-9A-Za-z\u0410-\u042F\u0430-\u044F]{2,30}" />
						<form:hidden path="id" value="${tag.id}" />
						<input id="updateButton${tag.id}" disabled type="submit"
							value="Update" class="btn btn-secondary" />
					</form:form></td>

				<td><form:form method="POST" action="deleteTag">
						<input name="tagId" type="hidden" value="${tag.id}" />
						<input type="submit" value="Delete" class="btn btn-danger" />
					</form:form></td>
			</tr>
		</c:forEach>
	</table>
	<p>Add tag:</P>
	<form:form method="POST" action="addTag">
		<input type="text" name="tagName" size="30"
			pattern="[(\s){1}0-9A-Za-z\u0410-\u042F\u0430-\u044F]{2,30}" required />
		<input type="submit" value="Save" class="btn btn-success" />
	</form:form>
	<p>
		<c:out value="${errorTag}" />
	</p>
</div>
