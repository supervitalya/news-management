<%@ page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<script type="text/javascript">
	function showUpdate(i) {
		var element = document.getElementById("updateButton" + i);
		element.removeAttribute("disabled");
		element.className="btn btn-info";
	}
</script>

<div class="body">
	<div class="page-header">

		<h1>Authors list</h1>
	</div>
	<table>
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Expired</th>
		</tr>
		<c:forEach var="author" items="${authorsList}">
			<tr>
				<form:form method="POST" modelAttribute="author"
					action="updateAuthor">
					<td><form:input type="text" path="id" size="7" readonly="true"
							value="${author.id}" /></td>
					<c:choose>
						<c:when test="${not empty author.expired}">
							<td><form:input type="text" path="authorName"
									value="${author.authorName}" disabled="true" /></td>
						</c:when>
						<c:otherwise>
							<td><form:input type="text" path="authorName"
									value="${author.authorName}"
									onChange="showUpdate(${author.id})"
									pattern="[(\s){1}A-Za-z\u0410-\u042F\u0430-\u044F]{2,30}" /></td>
						</c:otherwise>
					</c:choose>

					<td><form:input type="text" path="expired" disabled="true"
							value="${author.expired}" /></td>
					<c:if test="${empty author.expired}">
						<td><input id="updateButton${author.id}" disabled
							type="submit" value="Update" class="btn btn-secondary" /></td>

					</c:if>
				</form:form>
				<c:if test="${not empty author.expired}">

					<td><form:form method="POST" action="deleteAuthor">
							<input name="authorId" type="hidden" value="${author.id}" />
							<input type="submit" value="Delete" class="btn btn-danger" />
						</form:form></td>
				</c:if>
				<c:choose>
					<c:when test="${empty author.expired}">
						<td><form:form method="POST" action="expireAuthor">
								<input name="authorId" type="hidden" value="${author.id}" />
								<input type="submit" value="Set expired" class="btn btn-success" />
							</form:form></td>

					</c:when>
					<c:otherwise></c:otherwise>
				</c:choose>
				<c:if test="${empty author.expired}">
					<td><form:form method="POST" action="deleteAuthor">
							<input name="authorId" type="hidden" value="${author.id}" />
							<input type="submit" value="Delete" class="btn btn-danger" />
						</form:form></td>
				</c:if>
			</tr>
		</c:forEach>
	</table>

	<p>Add author:</P>
	<form:form method="POST" action="addAuthor">
		<input type="text" name="authorName" size="30"
			pattern="[(\s){1}A-Za-z\u0410-\u042F\u0430-\u044F]{2,30}" required />
		<input type="submit" value="Save" class="btn btn-success" />
	</form:form>

</div>
