package com.epam.newsmanagement.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsmanagement.controller.util.Parameter;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;

/**
 * Controller performs actions related to work with authors.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
@Controller
public class AuthorController {
	@Autowired
	private AuthorService authorService;

	@RequestMapping(value = "/addAuthor", method = RequestMethod.GET)
	public ModelAndView openAddAuthorPage(ModelMap modelMap) {
		ModelAndView mav = new ModelAndView(Parameter.ADD_AUTHOR);
		if (modelMap.containsAttribute(Parameter.AUTHOR)) {
			mav.addObject(Parameter.AUTHOR, modelMap.get(Parameter.AUTHOR));
		} else {
			mav.addObject(Parameter.AUTHOR, new Author());
		}
		return mav;
	}

	@RequestMapping(value = "/addAuthor", method = RequestMethod.POST)
	public String addAuthor(@ModelAttribute(Parameter.AUTHOR) @Valid Author author, BindingResult result,
			RedirectAttributes redirect) throws ServiceException {
		if (result.hasErrors()) {
			redirect.addFlashAttribute(Parameter.AUTHOR, author);
			redirect.addFlashAttribute(Parameter.BINDING_RESULT + "tag", result);
			return Parameter.REDIRECT_AUTHORS;
		}
		authorService.addAuthor(author);
		return Parameter.REDIRECT_AUTHORS;
	}

	@RequestMapping(value = "/authors", method = RequestMethod.GET)
	public ModelAndView openAuthors() throws ServiceException {
		List<Author> authorsList = null;
		ModelAndView mav = new ModelAndView();
		mav.setViewName(Parameter.AUTHORS);
		mav.addObject(Parameter.AUTHOR, new Author());
		authorsList = authorService.fetchAllAuthors();
		mav.addObject(Parameter.AUTHORS_LIST, authorsList);
		return mav;
	}

	@RequestMapping(value = "/expireAuthor", method = RequestMethod.POST)
	public String expireAuthor(@RequestParam(Parameter.AUTHOR_ID) Long authorId) throws ServiceException {
		authorService.setAuthorExpired(authorId);
		return Parameter.REDIRECT_AUTHORS;
	}

	@RequestMapping(value = "/updateAuthor", method = RequestMethod.POST)
	public String updateAuthor(@ModelAttribute(Parameter.AUTHOR) Author author) throws ServiceException {
		authorService.update(author);
		return Parameter.REDIRECT_AUTHORS;
	}

	@RequestMapping(value = "/deleteAuthor", method = RequestMethod.POST)
	public String deleteAuthor(@RequestParam(Parameter.AUTHOR_ID) Long authorId) throws ServiceException {
		authorService.remove(authorId);
		return Parameter.REDIRECT_AUTHORS;
	}
}