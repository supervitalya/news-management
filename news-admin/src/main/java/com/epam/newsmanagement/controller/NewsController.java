package com.epam.newsmanagement.controller;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsmanagement.controller.util.Parameter;
import com.epam.newsmanagement.dao.criteria.SearchCriteria;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.NewsDTO;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

@Controller
public class NewsController {
	@Autowired
	private NewsService newsService;
	@Autowired
	private TagService tagService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private CommentService commentService;

	@RequestMapping(value = "/singleNews")
	public ModelAndView openNews(@RequestParam("id") Long newsId, ModelMap modelMap) throws ServiceException {
		ModelAndView mav = new ModelAndView(Parameter.NEWS);
		boolean contains = false;
		if (modelMap.containsAttribute(Parameter.NEWS)) {
			mav.addObject(Parameter.NEWS_DTO, modelMap.get(Parameter.NEWS));
			contains = true;
		}
		List<Tag> allTags = Collections.emptyList();
		List<Author> allAuthors = Collections.emptyList();

		if (!contains) {
			NewsDTO newsDto = new NewsDTO();
			News news = newsService.fetchNews(newsId);
			Author author = authorService.fetchByNewsId(newsId);
			List<Tag> tagsList = tagService.fetchByNewsId(newsId);
			List<Comment> commentsList = commentService.fetchByNewsId(newsId);
			newsDto.setAuthor(author);
			newsDto.setCommentsList(commentsList);
			newsDto.setNews(news);
			newsDto.setTagsList(tagsList);
			mav.addObject(Parameter.NEWS_DTO, newsDto);
		}

		allTags = tagService.fetchAllTags();
		allAuthors = authorService.fetchNotExpiredAuthors();
		Long nextNewsId = newsService.fetchNextNewsId(newsId);
		Long previousNewsId = newsService.fetchPreviousNewsId(newsId);
		mav.addObject(Parameter.NEXT_NEWS_ID, nextNewsId);
		mav.addObject(Parameter.PREVIOUS_NEWS_ID, previousNewsId);
		mav.addObject(Parameter.ALL_TAGS, allTags);
		mav.addObject(Parameter.ALL_AUTHORS, allAuthors);
		return mav;

	}

	@Transactional(rollbackFor = Exception.class)
	@RequestMapping(value = "/addNews", method = RequestMethod.POST)
	public String addNews(@ModelAttribute(Parameter.NEWS) @Valid NewsDTO newsDTO, BindingResult bindingResult,
			Model model, @RequestParam("authorId") Long authorId, @RequestParam("tagsIdList") List<Long> tagsIdList,
			RedirectAttributes redirectAttributes) throws ServiceException {
		if (bindingResult.hasErrors()) {
			redirectAttributes.addFlashAttribute(Parameter.NEWS, newsDTO);
			redirectAttributes.addFlashAttribute(Parameter.BINDING_RESULT + Parameter.NEWS, bindingResult);
			return Parameter.REDIRECT_ADD_NEWS;
		}
		Long currentTime = System.currentTimeMillis();
		News news = newsDTO.getNews();
		news.setCreationDate(new Timestamp(currentTime));
		news.setModificationDate(new Date(currentTime));
		Long newsId = newsService.addNews(news);
		newsService.addTagsToNews(newsId, tagsIdList);
		newsService.addAuthorToNews(newsId, authorId);
		return Parameter.REDIRECT_SEARCH;
	}

	@RequestMapping(value = "/addNews", method = RequestMethod.GET)
	public ModelAndView openNewsAddPage(ModelMap modelMap) throws ServiceException {
		ModelAndView mav;
		if (modelMap.containsAttribute(Parameter.NEWS)) {
			mav = new ModelAndView(Parameter.ADD_NEWS, Parameter.NEWS, modelMap.get(Parameter.NEWS));
		} else {
			mav = new ModelAndView(Parameter.ADD_NEWS, Parameter.NEWS, new NewsDTO());
		}
		List<Tag> allTags = Collections.emptyList();
		List<Author> allAuthors = Collections.emptyList();
		allTags = tagService.fetchAllTags();
		allAuthors = authorService.fetchNotExpiredAuthors();
		modelMap.addAttribute(Parameter.ALL_TAGS, allTags);
		modelMap.addAttribute(Parameter.ALL_AUTHORS, allAuthors);
		return mav;
	}

	@Transactional(rollbackFor = Exception.class)
	@RequestMapping(value = "/removeNews")
	public String removeNews(@RequestParam("id") Long newsId, Model model) throws ServiceException {
		newsService.removeNews(newsId);
		newsService.removeAllTagsFromNews(newsId);
		newsService.removeAuthorFromNews(newsId);
		return Parameter.REDIRECT_SEARCH;
	}

	@Transactional(rollbackFor = Exception.class)
	@RequestMapping(value = "/updateNews")
	public String updateNews(@RequestParam(value = "tagsIdList", required = false) List<Long> tagsIdList,
			@RequestParam(value = "authorId", required = false) Long authorId,
			@ModelAttribute(Parameter.NEWS) @Valid NewsDTO newsDTO, BindingResult result, RedirectAttributes redirect)
			throws ServiceException {
		Long newsId = newsDTO.getNews().getId();
		if (result.hasErrors()) {
			redirect.addFlashAttribute(Parameter.BINDING_RESULT + Parameter.NEWS, result);
			redirect.addFlashAttribute(Parameter.NEWS, newsDTO);
			return Parameter.REDIRECT_SINGLE_WITH_ID + newsId;
		}
		News currentNews = newsDTO.getNews();

		News oldNews = newsService.fetchNews(newsId);
		oldNews.setTitle(currentNews.getTitle());
		oldNews.setShortText(currentNews.getShortText());
		oldNews.setFullText(currentNews.getFullText());

		if (authorId != null) {
			newsService.updateNewsAuthor(newsId, authorId);
		}

		if (tagsIdList != null) {
			newsService.updateTags(newsId, tagsIdList);
		}

		oldNews.setModificationDate(new Date(System.currentTimeMillis()));
		newsService.editNews(oldNews);
		return Parameter.REDIRECT_SINGLE_WITH_ID + newsId;

	}

	@RequestMapping(value = { "/search" }, method = RequestMethod.GET)
	public ModelAndView search(@ModelAttribute("searchCriteria") SearchCriteria criteria,
			@RequestParam(value = "page", required = false, defaultValue = "1") Long page,
			@RequestParam(value = "tagsIdList", required = false) List<Long> tagsIdList,
			@RequestParam(value = "authorsIdList", required = false) List<Long> authorsIdList, Model model,
			HttpServletRequest request) throws ServiceException {
		HttpSession session = request.getSession();
		List<Tag> allTags = Collections.emptyList();
		List<Author> allAuthors = Collections.emptyList();
		allTags = tagService.fetchAllTags();
		allAuthors = authorService.fetchAllAuthors();
		model.addAttribute(Parameter.ALL_TAGS, allTags);
		model.addAttribute(Parameter.ALL_AUTHORS, allAuthors);

		String sorting = request.getParameter(Parameter.SPECIFY_SORTING);
		boolean sentFromSearchForm = "true".equals(sorting);
		boolean sortedByComments;
		Object sortedAttr = session.getAttribute(Parameter.SORTED);
		if (sortedAttr != null) {
			sortedByComments = Boolean.parseBoolean(sortedAttr.toString());
		} else {
			sortedByComments = false;
		}

		if (sentFromSearchForm) {
			sortedByComments = criteria.isSortedByComments();
			session.setAttribute(Parameter.SORTED, sortedByComments);
		}
		Object criteriaObject = session.getAttribute(Parameter.CRITERIA);
		SearchCriteria newCriteria;
		if (criteriaObject != null) {
			newCriteria = (SearchCriteria) criteriaObject;
		} else {
			newCriteria = new SearchCriteria();
		}
		newCriteria.setSortedByComments(sortedByComments);
		newCriteria.setAmount(Parameter.NEWS_ON_PAGE);
		newCriteria.setPage(page);
		if (sentFromSearchForm) {
			newCriteria.setTagsIdList(tagsIdList);
			newCriteria.setAuthorsIdList(authorsIdList);
		} else {
			if (((tagsIdList != null)) && (!tagsIdList.isEmpty())) {
				newCriteria.setTagsIdList(tagsIdList);
			}
			if (((authorsIdList != null)) && (!authorsIdList.isEmpty())) {
				newCriteria.setAuthorsIdList(authorsIdList);
			}
		}
		ModelAndView mav = new ModelAndView();
		session.setAttribute(Parameter.CRITERIA, newCriteria);
		mav.setViewName(Parameter.HOME);
		List<News> newsList = new ArrayList<>();
		newsList = newsService.searchByCriteria(newCriteria);
		mav.addObject(Parameter.NEWS_LIST, newsList);
		double newsNumber = newsService.fetchByCriteriaAmount(newCriteria);
		double pagesNumberDoubleValue = newsNumber / Parameter.NEWS_ON_PAGE;
		long pagesNumber = (long) Math.ceil(pagesNumberDoubleValue);
		mav.addObject(Parameter.PAGES_AMOUNT, pagesNumber);
		return mav;
	}

	@RequestMapping(value = "/deleteComment", method = RequestMethod.POST)
	public String deleteComment(@RequestParam("commentId") Long commentId, @RequestParam("newsId") Long newsId)
			throws ServiceException {
		commentService.removeComment(commentId);
		return Parameter.REDIRECT_SINGLE_WITH_ID + newsId;
	}

	@Transactional(rollbackFor = Exception.class)
	@RequestMapping(value = "/deleteNews")
	public String deleteNews(@RequestParam("newsId") Long newsId) throws ServiceException {
		newsService.removeAuthorFromNews(newsId);
		newsService.removeAllTagsFromNews(newsId);
		commentService.removeAllComments(newsId);
		newsService.removeNews(newsId);
		return Parameter.REDIRECT_SEARCH;
	}

	@RequestMapping(value = "/resetSearch", method = RequestMethod.POST)
	public String resetSearch(HttpServletRequest request) throws ServiceException {
		HttpSession session = request.getSession();
		session.removeAttribute(Parameter.SORTED);
		session.removeAttribute(Parameter.CRITERIA);
		return Parameter.REDIRECT_SEARCH;
	}
}
