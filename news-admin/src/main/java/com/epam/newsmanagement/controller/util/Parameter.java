package com.epam.newsmanagement.controller.util;

/**
 * Contains all constants, pages addresses, requests, parameter names and etc.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
public class Parameter {

	public static final int NEWS_ON_PAGE = 5;
	public static final String AUTHOR = "author";
	public static final String AUTHORS = "authors";
	public static final String REDIRECT_AUTHORS = "redirect:authors";
	public static final String ERROR = "error";
	public static final String AUTHOR_ID = "authorId";
	public static final String BINDING_RESULT = "org.springframework.validation.BindingResult.";
	public static final String ADD_AUTHOR = "addAuthor";
	public static final String AUTHORS_LIST = "authorsList";
	public static final String TAG = "tag";
	public static final String REDIRECT_TAGS = "redirect:tags";
	public static final String TAGS = "tags";
	public static final String TAGS_LIST = "tagsList";
	public static final String TAGS_ID_LIST = "tagsIdList";
	public static final String TAG_ID = "tagId";
	public static final String REDIRECT_LOGIN = "redirect:/login";
	public static final String NEWS = "news";
	public static final String NEWS_LIST = "newsList";
	public static final String ALL_AUTHORS = "allAuthors";
	public static final String ALL_TAGS = "allTags";
	public static final String REDIRECT_SEARCH = "redirect:search";
	public static final String NEWS_DTO = "newsDTO";
	public static final String REDIRECT_SINGLE_WITH_ID = "redirect:singleNews?id=";
	public static final String CRITERIA = "criteria";
	public static final String HOME = "home";
	public static final String ADD_NEWS = "addNews";
	public static final String PAGES_AMOUNT = "pagesAmount";
	public static final String REDIRECT_ADD_NEWS = "redirect:addNews";
	public static final String NEXT_NEWS_ID = "nextNewsId";
	public static final String PREVIOUS_NEWS_ID = "previousNewsId";
	public static final String SORTED = "sorted";
	public static final String SPECIFY_SORTING = "specifySorting";
	public static final String ERROR_TAG = "errorTag";
	public static final String ERROR_TAG_MESSAGE = "Tag already exist";

	private Parameter() {
	}
}
