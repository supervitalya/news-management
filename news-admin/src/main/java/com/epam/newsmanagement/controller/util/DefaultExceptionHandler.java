package com.epam.newsmanagement.controller.util;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class DefaultExceptionHandler {
	private Logger LOG = Logger.getLogger(DefaultExceptionHandler.class);

	public DefaultExceptionHandler() {
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public String handlerException(Exception e) {
		LOG.error("Internal server error " + e);
		LOG.info("DEFAULT HANDLER");
		return Parameter.ERROR;
	}
}
