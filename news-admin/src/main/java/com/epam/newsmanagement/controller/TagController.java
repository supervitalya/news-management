package com.epam.newsmanagement.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsmanagement.controller.util.Parameter;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;

@Controller
public class TagController {
	@Autowired
	private TagService tagService;

	@RequestMapping(value = "/tags", method = RequestMethod.GET)
	public ModelAndView openTags(ModelMap modelMap) throws ServiceException {
		ModelAndView mav = new ModelAndView(Parameter.TAGS);
		if (modelMap.containsAttribute(Parameter.TAG)) {
			mav.addObject(Parameter.TAG, modelMap.get(Parameter.TAG));
		} else {
			mav.addObject(Parameter.TAG, new Tag());
		}
		List<Tag> tagsList = null;
		tagsList = tagService.fetchAllTags();
		mav.addObject(Parameter.TAGS_LIST, tagsList);
		return mav;
	}

	@RequestMapping(value = "/addTag", method = RequestMethod.POST)
	public String addTag(@ModelAttribute(Parameter.TAG) @Valid Tag tag, BindingResult result,
			RedirectAttributes redirect) throws ServiceException {
		if (result.hasErrors()) {
			redirect.addFlashAttribute(Parameter.TAG, tag);
			redirect.addFlashAttribute(Parameter.BINDING_RESULT + Parameter.TAG, result);
			return Parameter.REDIRECT_TAGS;
		}
		try {
			tagService.addTag(tag);
		} catch (ServiceException e) {
			redirect.addFlashAttribute(Parameter.TAG, tag);
			redirect.addFlashAttribute(Parameter.ERROR_TAG, Parameter.ERROR_TAG_MESSAGE);
			return Parameter.REDIRECT_TAGS;
		}
		return Parameter.REDIRECT_TAGS;
	}

	@RequestMapping(value = "/updateTag", method = RequestMethod.POST)
	public String updateTag(@ModelAttribute(Parameter.TAG) Tag tag) throws ServiceException {
		tagService.updateTag(tag);
		return Parameter.REDIRECT_TAGS;
	}

	@RequestMapping(value = "/deleteTag", method = RequestMethod.POST)
	public String deleteTag(@RequestParam(Parameter.TAG_ID) Long tagId) throws ServiceException {
		tagService.removeTag(tagId);
		return Parameter.REDIRECT_TAGS;
	}
}