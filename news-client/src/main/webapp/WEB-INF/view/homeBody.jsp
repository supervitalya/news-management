<%@ page errorPage="error.jsp" %>
<%@ page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<div class="body">
	<p class="articleTitle">Here is news list.</P>
	<table>
		
		<tr>
			<td><form method="GET">
					<input type="hidden" name="command" value="searchByCriteriaCommand">
					<input type="hidden" name="specifySorting" value="true">
					<p>Tags:</p>
					<select name="tags" multiple>
						<c:forEach var="tag" items="${tagsList}">
							<option value="${tag.id}"><c:out value="${tag.tagName}" /></option>
						</c:forEach>
					</select> </td><td><p>Authors:</p><select name="authors" multiple>
						<c:forEach var="author" items="${authorsList}">
							<option value="${author.id}"><c:out
									value="${author.authorName}" /></option>
						</c:forEach>
					</select></td><td>Sort by comments:<c:choose>
						<c:when test="${isSorted}">
							<input type="checkbox" name="isSorted" value="true" checked/>
						</c:when>
						<c:otherwise>
							<input type="checkbox" name="isSorted" value="true" />
						</c:otherwise>
					</c:choose></td><td><table><tr><td> <input type="submit" value="Open" class="btn btn-success"/></td>
				</form><td>
				<form method="POST" action="Controller">
					<input type="hidden" name="command" value="resetSearchCommand"><input
						type="submit" value="Reset" class="btn btn-danger">
				</form></td></tr></table></td>
		</tr>
	</table>

	<hr />
	<table>
		<c:forEach var="news" items="${newsList}">
			<tr>
				<td><c:out value="${news.id}" /></td>
				<td><c:out value="${news.title}" /></td>
				<td><c:out value="${news.shortText}" /></td>
			</tr>
			<tr>
				<td><a href="news?command=openNewsCommand&newsId=${news.id}">Open</a></td>
			</tr>
		</c:forEach>
	</table>
	<ul class="pagination">
		<c:forEach begin="1" end="${pagesNumber}" varStatus="loop">
			<li><a
				href="home?command=searchByCriteriaCommand&page=${loop.index}"><c:out
						value="${loop.index}" /></a></li>
		</c:forEach>
	</ul>
</div>
