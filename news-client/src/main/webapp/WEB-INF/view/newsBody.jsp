<%@ page errorPage="error.jsp"%>
<%@ page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="newsBody">
	<div class="nextPrevLinks">
		<c:if test="${nextId!=0}">
			<a href="news?command=openNewsCommand&newsId=${nextId}"
				class="btn btn-success">Next </a>
		</c:if>
		<c:if test="${previousId!=0}">
			<a href="news?command=openNewsCommand&newsId=${previousId}"
				class="btn btn-success">Previous</a>
		</c:if>
	</div>
	<table>
		<tr>
			<td>Tags:</td>
			<td class="tags"><c:forEach var="tag"
					items="${newsDTO.tagsList}">
					<c:out value="${tag.tagName}" />
				</c:forEach></td>
		</tr>
		<tr>
			<td>News created by:</td>
			<td><c:out value="${newsDTO.author.authorName}" /></td>
		</tr>
		<tr>
			<td>Creation Date:</td>
			<td><fmt:formatDate pattern="yyyy-MM-dd hh:mm a"
					value="${newsDTO.news.creationDate}" /></td>
		</tr>
		<tr>
			<td>Modification Date:</td>
			<td><c:out value="${newsDTO.news.modificationDate}" /></td>
		</tr>
	</table>
	<p>
		<span class="articleTitle"><c:out value="${newsDTO.news.title}" /></span>
	</p>
	<p>
		<c:out value="${newsDTO.news.fullText}" />
	</p>
	<div class="commentsList">
		<table>
			<tr>
				<td>Comments:</td>
			</tr>
			<c:forEach var="comment" items="${newsDTO.commentsList}">
				<tr>
					<td><fmt:formatDate pattern="yyyy-MM-dd"
							value="${comment.creationDate}" /></td>
					<td><c:out value="${comment.commentText}" /></td>
				</tr>
			</c:forEach>
			<tr><td><c:out value="${error}"/></td></tr>
			<tr>
				<td>
					<form action="Controller" method="POST">
						<input type="hidden" name="command" value="addCommentCommand">
						<input type="hidden" name="newsId" value="${newsDTO.news.id}">
						<input type="text" name="commentText" maxlength="100"> <input
							type="submit" value="Submit" class="btn btn-success">
					</form>
				</td>
			</tr>
		</table>
	</div>
</div>