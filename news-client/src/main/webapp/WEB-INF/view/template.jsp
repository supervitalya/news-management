<%@ page errorPage="error.jsp"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<title>News service</title>
<style>
body {
	background: white;
}

.article {
	width: 80%;
	background: #DEDEDE;
	margin: 0 auto;
	font-size: 14pt;
	padding: 5% 5%;
	min-height: 700px;
}

.articleTitle {
	font-size: 16pt;
	font-weight: bold;
	font-color: #c0c0c0;
	text-align: center;
}

.tags {
	color: darkblue;
}

td {
	padding: 5px 30px;
}

.sidebar {
	padding: 1% 1%;
	float: left;
	width: 10%;
	min-height: 400px;
	background-color: coral;
	font-size: 14pt;
	font-weight: bold;
}

.sidebar a {
	text-decoration: none;
}

.page {
	margin-top: 7%;
	margin-left: 3%;
}

.header {
	position: absolute;
	top: 0px;
	min-height: 50px;
	left: 25%;
	width: 50%;
	text-align: center;
	font-weight: bold;
	font-size: 14pt;
	text-transform: uppercase;
	background-color: lightgreen;
}

.footer {
	margin: 0 auto;
	position: relative;
	bottom: 0px;
	min-height: 50px;
	width: 50%;
	text-align: center;
	font-weight: bold;
	font-size: 14pt;
	text-transform: uppercase;
	background-color: lightblue;
}

.commentsList {
	margin-top: 50px;
}

.nextPrevLinks {
	width: 50%;
	height: 30px;
	margin: 2% 10%;
}

</style>
</head>
<body>
	<div class="page">
		<tiles:insertAttribute name="header" />
		<div class="content">
			<tiles:insertAttribute name="sidebar" />
			<div class="article">
				<tiles:insertAttribute name="body" />
			</div>
		</div>
		<tiles:insertAttribute name="footer" />
	</div>
</body>
</html>