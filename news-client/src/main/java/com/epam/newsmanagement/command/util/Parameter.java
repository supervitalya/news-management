package com.epam.newsmanagement.command.util;

/**
 * Contains all constants, pages addresses, parameter names and etc. for
 * client's side.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
public class Parameter {
	public static final int DEFAULT_AMOUNT = 5;
	public static final int DEFAULT_PAGE = 1;
	public static final int NEWS_ON_PAGE = 5;
	public static final String NEWS_PAGE = "WEB-INF/view/news.jsp";
	public static final String HOME_PAGE = "WEB-INF/view/home.jsp";
	public static final String ERROR_PAGE = "WEB-INF/view/error.jsp";
	public static final String PAGES_NUMBER = "pagesNumber";
	public static final String NEWS_LIST = "newsList";
	public static final String AUTHORS_LIST = "authorsList";
	public static final String TAGS_LIST = "tagsList";
	public static final String CRITERIA = "criteria";
	public static final String TAGS = "tags";
	public static final String AUTHORS = "authors";
	public static final String PAGE = "page";
	public static final String NEXT_ID = "nextId";
	public static final String PREVIOUS_ID = "previousId";
	public static final String NEWS_DTO = "newsDTO";
	public static final String NEWS_ID = "newsId";
	public static final String COMMENT_TEXT = "commentText";
	public static final String COMMAND = "command";
	public static final String CONTEXT = "context.xml";
	public static final String IS_SORTED = "isSorted";
	public static final String OPEN_NEWS_COMMAND = "Controller?command=openNewsCommand&";
	public static final String SEARCH_COMMAND = "Controller?command=searchByCriteriaCommand&";
	public static final int MAX_COMMENT_LENGTH = 100;
	public static final String ERROR_MESSAGE = "Comment text is incorrect.";
	public static final String ERROR = "error";
	public static final String SPECIFY_SORTING = "specifySorting";

	private Parameter() {
	}
}
