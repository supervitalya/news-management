package com.epam.newsmanagement.command.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;

import com.epam.newsmanagement.command.Command;
import com.epam.newsmanagement.command.util.Parameter;
import com.epam.newsmanagement.dao.criteria.SearchCriteria;
import com.epam.newsmanagement.exception.CommandException;

/**
 * Replace SearchCriteria object from session with an empty SearchCriteria
 * object.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
@Component("resetSearchCommand")
public class ResetSearchCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		HttpSession session = request.getSession();
		SearchCriteria emptyCriteria = new SearchCriteria();
		emptyCriteria.setAmount(Parameter.DEFAULT_AMOUNT);
		emptyCriteria.setPage(Parameter.DEFAULT_PAGE);
		session.removeAttribute(Parameter.IS_SORTED);
		session.setAttribute(Parameter.CRITERIA, emptyCriteria);
		return Parameter.SEARCH_COMMAND + Parameter.CRITERIA + "=" + emptyCriteria;
	}
}
