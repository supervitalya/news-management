package com.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagement.exception.CommandException;

/**
 * Performs actions using http request.
 * 
 * @author Vitaly_Blyaharchuk
 */
public interface Command {
	/**
	 * Performs some actions depending on Command attribute of http request.
	 * 
	 * @param request
	 *            http request
	 * @return page addree
	 * @throws CommandException
	 *             if something goes wrong with Service layer
	 */
	String execute(HttpServletRequest request) throws CommandException;
}
