package com.epam.newsmanagement.command.util;

/**
 * Utility class that performs comment text validation.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
public class CommentValidator {

	private CommentValidator() {
	}

	/**
	 * Validates comment text.
	 * 
	 * @param comment
	 *            text
	 * @return true if comment text is valid
	 */
	public static boolean isValid(String comment) {
		if ((comment == null) || (comment.isEmpty())) {
			return false;
		}
		if (comment.length() > 100) {
			return false;
		}

		return true;
	}
}
