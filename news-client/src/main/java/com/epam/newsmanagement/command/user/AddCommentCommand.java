package com.epam.newsmanagement.command.user;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.command.Command;
import com.epam.newsmanagement.command.util.CommentValidator;
import com.epam.newsmanagement.command.util.Parameter;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;

/**
 * Adds Comment to News. Requires validation of comment's text.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
@Component("addCommentCommand")
public class AddCommentCommand implements Command {
	@Autowired
	CommentService commentService;

	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		Long newsId = Long.parseLong(request.getParameter(Parameter.NEWS_ID));
		String commentText = request.getParameter(Parameter.COMMENT_TEXT);
		if (!CommentValidator.isValid(commentText)) {
			request.setAttribute(Parameter.ERROR, Parameter.ERROR_MESSAGE);
			return Parameter.OPEN_NEWS_COMMAND + Parameter.NEWS_ID + "=" + newsId + "&" + Parameter.ERROR + "=true";
		}
		long currentTime = System.currentTimeMillis();
		Comment comment = new Comment();
		comment.setCommentText(commentText);
		comment.setNewsId(newsId);
		comment.setCreationDate(new Timestamp(currentTime));
		try {
			commentService.addComment(comment);
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		return Parameter.OPEN_NEWS_COMMAND + Parameter.NEWS_ID + "=" + newsId;
	}

}
