package com.epam.newsmanagement.command.user;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.command.Command;
import com.epam.newsmanagement.command.util.Parameter;
import com.epam.newsmanagement.dao.criteria.SearchCriteria;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

/**
 * Performs search according to criteria. Criteria should be specified in
 * request. If it isn't there will be empty SearchCriteria created.
 * 
 * @see com.epam.newsmanagement.dao.criteria.SearchCriteria
 * @see com.epam.newsmanagement.dao.criteria.SearchCriteriaBuilder
 * @author Vitaly_Blyaharchuk
 *
 */
@Component("searchByCriteriaCommand")
public class SearchByCriteriaCommand implements Command {
	@Autowired
	NewsService newsService;
	@Autowired
	TagService tagService;
	@Autowired
	AuthorService authorService;

	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		HttpSession session = request.getSession();
		Object criteriaObject = session.getAttribute(Parameter.CRITERIA);
		SearchCriteria criteria;
		if (criteriaObject != null) {
			criteria = (SearchCriteria) criteriaObject;
		} else {
			criteria = new SearchCriteria();
		}
		criteria.setAmount(Parameter.DEFAULT_AMOUNT);
		String[] tagIds = request.getParameterValues(Parameter.TAGS);
		String[] authorIds = request.getParameterValues(Parameter.AUTHORS);
		String pageNumber = request.getParameter(Parameter.PAGE);
		String changeSorting = request.getParameter(Parameter.SPECIFY_SORTING);
		String isSorted = request.getParameter(Parameter.IS_SORTED);
		boolean sentFromSearchForm = "true".equals(changeSorting);
		if (sentFromSearchForm) {
			boolean sorted = Boolean.parseBoolean(isSorted);
			criteria.setSortedByComments(sorted);
			request.getSession().setAttribute(Parameter.IS_SORTED, isSorted);
		}
		long page;
		if (pageNumber != null) {
			page = Long.parseLong(pageNumber);
		} else {
			page = Parameter.DEFAULT_PAGE;
		}
		criteria.setPage(page);
		List<Long> tagsIdList = new ArrayList<>();
		List<Long> authorsIdList = new ArrayList<>();
		try {
			tagsIdList = stringArrayToListLong(tagIds);
			authorsIdList = stringArrayToListLong(authorIds);
		} catch (ServiceException e) {
			throw new CommandException(e);
		}

		if (sentFromSearchForm) {
			criteria.setAuthorsIdList(authorsIdList);
			criteria.setTagsIdList(tagsIdList);
		} else {
			if (!authorsIdList.isEmpty()) {
				criteria.setAuthorsIdList(authorsIdList);
			}
			if (!tagsIdList.isEmpty()) {
				criteria.setTagsIdList(tagsIdList);
			}
		}
		session.setAttribute(Parameter.CRITERIA, criteria);

		List<Author> authorsList = Collections.emptyList();
		List<Tag> tagsList = Collections.emptyList();

		try {
			authorsList = authorService.fetchAllAuthors();
			tagsList = tagService.fetchAllTags();
			List<News> newsList = newsService.searchByCriteria(criteria);
			double newsNumber = newsService.fetchByCriteriaAmount(criteria);
			double pagesNumberDoubleValue = newsNumber / Parameter.NEWS_ON_PAGE;
			long pagesNumber = (long) Math.ceil(pagesNumberDoubleValue);
			session.setAttribute(Parameter.PAGES_NUMBER, pagesNumber);
			request.setAttribute(Parameter.NEWS_LIST, newsList);
			request.setAttribute(Parameter.AUTHORS_LIST, authorsList);
			request.setAttribute(Parameter.TAGS_LIST, tagsList);
			request.setAttribute(Parameter.IS_SORTED, criteria.isSortedByComments());
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		return Parameter.HOME_PAGE;
	}

	private List<Long> stringArrayToListLong(String[] array) throws ServiceException {
		List<Long> list = new ArrayList<>();
		if ((array == null) || (array.length == 0)) {
			return list;
		}
		try {
			list = Arrays.stream(array).map(Long::parseLong).collect(Collectors.toList());
		} catch (NumberFormatException e) {
			throw new ServiceException(e);
		}
		return list;
	}

}
