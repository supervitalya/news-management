package com.epam.newsmanagement.command.user;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.command.Command;
import com.epam.newsmanagement.command.util.Parameter;
import com.epam.newsmanagement.domain.NewsDTO;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

@Component("openNewsCommand")
public class OpenNewsCommand implements Command {

	@Autowired
	NewsService newsService;
	@Autowired
	AuthorService authorService;
	@Autowired
	TagService tagService;
	@Autowired
	CommentService commentService;

	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		Long newsId = Long.parseLong(request.getParameter(Parameter.NEWS_ID));
		Object errorAttr = request.getAttribute(Parameter.ERROR);
		if (errorAttr != null) {
			request.setAttribute(Parameter.ERROR, Parameter.ERROR_MESSAGE);
		}
		try {
			NewsDTO newsDTO = new NewsDTO();
			newsDTO.setNews(newsService.fetchNews(newsId));
			newsDTO.setTagsList(tagService.fetchByNewsId(newsId));
			newsDTO.setAuthor(authorService.fetchByNewsId(newsId));
			newsDTO.setCommentsList(commentService.fetchByNewsId(newsId));
			Long nextId = newsService.fetchNextNewsId(newsId);
			request.setAttribute(Parameter.NEXT_ID, nextId);
			Long previousId = newsService.fetchPreviousNewsId(newsId);
			request.setAttribute(Parameter.PREVIOUS_ID, previousId);
			request.setAttribute(Parameter.NEWS_DTO, newsDTO);
		} catch (ServiceException e) {
			throw new CommandException(e);
		}

		return Parameter.NEWS_PAGE;
	}

}
