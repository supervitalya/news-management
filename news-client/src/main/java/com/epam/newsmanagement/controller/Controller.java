package com.epam.newsmanagement.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagement.command.Command;
import com.epam.newsmanagement.command.util.Parameter;
import com.epam.newsmanagement.exception.CommandException;

@SuppressWarnings({ "serial" })
/**
 * Client-side application controller. Performs the same actions for doGet and
 * doPost requests.
 * 
 * @author Vitaly_Blyaharchuk
 *
 */
public class Controller extends HttpServlet {
	private Logger log = Logger.getLogger(Controller.class);
	private ApplicationContext context;

	@Override
	public void init() {
		context = new ClassPathXmlApplicationContext(Parameter.CONTEXT);
	}

	/**
	 * Fetches Command request parameter, fetches Command entity from spring
	 * context according to it and executes it.
	 * 
	 * @param request
	 *            http request
	 * @param response
	 *            http response
	 * @return page address
	 * @throws ServletException
	 */
	private String processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String commandName = request.getParameter(Parameter.COMMAND);
		Command command = (Command) context.getBean(commandName);
		String page;
		try {
			page = command.execute(request);
		} catch (CommandException e) {
			log.error(e);
			page = Parameter.ERROR_PAGE;
		}
		return page;

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String page = processRequest(request, response);
		request.getRequestDispatcher(page).forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String page = processRequest(request, response);
		response.sendRedirect(page);
	}

}
