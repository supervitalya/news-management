#News-management

##Description
Hibernate and JPA DAOs implementation.
##Requirements
Implementation requirements:
- Introduce hibernate (use xml mapping) and EclipseLink or TopLink implementations for JPA api for all DAOs from 2-d task;
- Only internal implementation changes are expected in News Management application;
- Optimistic Lock concept have to be implemented for News Management application;
- The implementation of DAO is switched with help of configuration property at Tomcat level;
- Create script which generates 10.000 records for each table;
- Try to improve the performance of application using Hibernate Caching technique.

##Already implemented
- Hibernate DAO;
- EclipseLink DAO;

##TODO List
1. Sorting by comments.
2. Optimistic lock.
3. DB generation script.
4. Client-side validation (finish).
5. Changing DAO implementation using tomcat config.
6. Hibernate caching.
7. Dropdown list with checkboxes for selecting tags.
8. Fix update news' author